//System.Diagnostics.Debugger.Break();
//試驗結果與藥品批量的燈號顯示
//試驗報告的”試驗結果”與藥品批量的”批量試驗重點”燈號顯示
//in_Color_Drugs_Char

Innovator inn = this.getInnovator();
//string ThisType = this.getType();
for(int i=0;i<this.getItemCount();i++)
{
	//string bg_color ="";
	string text_color  = "#000000";
	Item thisItem = this.getItemByIndex(i);
	string thisItemType = thisItem.getAttribute("type","");
	string thisStatus = thisItem.getProperty("state","");
	
	string param = "<rel_type>" + thisItemType + "</rel_type>";
	
	param += "<id>" + thisItem.getID() + "</id>";
	param += "<condition_property>in_decision_condition</condition_property>";
	param += "<target_propertys>in_fda_standard,in_gcp,in_glp,in_gmp_standard,in_target</target_propertys>";
	param += "<base_property>in_testdata</base_property>";
	
	Item ItemColor = inn.applyMethod("in_VerifyTestResult",param);
	//Item r = ItemColor.getResult();
	string css="";
	XmlNodeList nodes = ItemColor.dom.SelectSingleNode("//Item").ChildNodes;
	foreach(XmlNode n in nodes)
	{
		css += "." + n.Name + " {background-color: " + n.InnerText + " } ";
	}
	thisItem.setProperty("css",css);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Color_Drugs_Char' and [Method].is_current='1'">
<config_id>720CF319329349C6B4E32B3CCED1E6DA</config_id>
<name>In_Color_Drugs_Char</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
