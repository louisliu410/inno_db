if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	var viewProc = false;
	if (workerFrame && workerFrame.grid) {
		var rowID = workerFrame.grid.getSelectedId();
		if (rowID) {
			var activityType = workerFrame.grid.getUserData(rowID, 'assignmentType');
			if (activityType == 'workflow' || activityType == 'project') {
				viewProc = true;
			}
		}
	}

	return {'cui_disabled': !viewProc};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_viewProcess' and [Method].is_current='1'">
<config_id>5D3CCB9654B84A9AB6050B8AAB32EF06</config_id>
<name>cui_reinit_mwt_viewProcess</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
