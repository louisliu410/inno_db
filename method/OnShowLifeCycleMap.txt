return ModulesManager.using(
	['aras.innovator.core.ItemWindow/LifeCycleItemWindowView',
	'aras.innovator.core.ItemWindow/DefaultItemWindowCreator']).then(function(View, Creator) {
		var view = new View(inDom,inArgs);
		var creator = new Creator(view);
		return creator.ShowView();
	});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnShowLifeCycleMap' and [Method].is_current='1'">
<config_id>726E1A4EABCA4E3E8CAA48FBA121DB84</config_id>
<name>OnShowLifeCycleMap</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
