var func = window.cmfContainer && window.cmfContainer.qpWindow && window.cmfContainer.qpWindow.documentEditor &&
	window.cmfContainer.qpWindow.documentEditor.recalculateComputedColumns;

if (!func) {
	aras.AlertWarning(aras.getResource('', 'cmf.recalculate_need_open_and_lock_editor'));
	return;
}

if (!parent.isEditMode) {
	aras.AlertWarning(aras.getResource('', 'cmf.recalculate_need_lock_editor'));
	return;
}

window.cmfContainer.qpWindow.documentEditor.recalculateComputedColumns();

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_RecalculateContentViews' and [Method].is_current='1'">
<config_id>2D8C42CE5298429E927A8AC162B61B06</config_id>
<name>cmf_RecalculateContentViews</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
