//System.Diagnostics.Debugger.Break();
//in_GetMyActivity
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string UserInfo = this.getProperty("userinfo","");
string r = "";
string criteria = this.getProperty("criteria","");
//_InnH.AddLog("UserInfo:" + UserInfo,"In_GetMyActivity");
 string Activity_criteria = "<UserID>" + inn.getUserID() + "</UserID>";
            Activity_criteria += "<ViewMode>Active</ViewMode>";
            Activity_criteria += "<WorkflowTasksFlag>1</WorkflowTasksFlag>";
            Activity_criteria += "<ProjectTasksFlag>0</ProjectTasksFlag>";
            Activity_criteria += "<ActionTasksFlag>0</ActionTasksFlag>";

Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
try
{
	r = _InnoApp.GetMyActivity(Activity_criteria);	
//	_InnH.AddLog("MyActivity:" + r,"In_GetMyActivity");
	string strFilterMethod = _InnoApp.GetParameterValue(criteria,"filter_method",'&',':',"");
	string strFilterCriteria = _InnoApp.GetParameterValue(criteria,"filter_criteria",'&',':',"");

	if(strFilterMethod!="")
	    r = inn.applyMethod(strFilterMethod,"<userinfo><![CDATA[" + UserInfo + "]]></userinfo><criteria><![CDATA[" + strFilterCriteria + "]]></criteria><content>" + System.Security.SecurityElement.Escape(r) + "</content>").getResult();
}
catch (Exception ex)
{
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}

//_InnH.AddLog("MyActivityFiltered:" + r,"In_GetMyActivity");

return  inn.newResult(r);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetMyActivity' and [Method].is_current='1'">
<config_id>16D4A886489346908797ACEEB1BB12CC</config_id>
<name>In_GetMyActivity</name>
<comments>Add By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
