	var rels = this.getRelationships();
	if (rels.getItemCount() == 0)
	{
		return this.getInnovator().newError("There is no items to select.");
	}

	var itemTypes = new List<ItemType>();
	for (var i = 0; i < rels.getItemCount(); i++)
	{
		var currentItemType = rels.getItemByIndex(i);
		var propsToSelect = currentItemType.getProperty("props_to_select");
		var properties = new List<string>();
		if (!string.IsNullOrEmpty(propsToSelect))
		{
			properties.AddRange(propsToSelect.Split(','));
		}

		itemTypes.Add(new ItemType
		{
			ItemTypeId = currentItemType.getID(),
			PropertiesList = properties
		});
	}

	if (itemTypes.Count == 0)
	{
		return this.getInnovator().newError("There is no items to select.");
	}

	var skipItemTypeProperties = !string.IsNullOrEmpty(this.getProperty("skipItemTypeProperties"));
	var settings = this.newItem("Method", "GetSSRSetting");
	settings = settings.apply();
	if (settings.isError())
	{
		return settings;
	}
	var excludedList = new List<Excluded>();
	var excludedprops = settings.getProperty("excluded_properties");
	if (!string.IsNullOrEmpty(excludedprops))
	{
		var items = excludedprops.Split('|');
		foreach (var item in items)
		{
			var parts = item.Split(':');
			var exc = excludedList.FirstOrDefault(x => string.Compare(x.ItemTypeId, parts[0], StringComparison.OrdinalIgnoreCase) == 0);
			if (exc != null)
			{
				exc.PropertiesList.Add(parts[1]);
			}
			else
			{
				excludedList.Add(new Excluded()
				{
					ItemTypeId = parts[0], PropertiesList = new List<string>()
					{
						parts[1]
					}
				});
			}
		}
	}

	return GetItemTypeWithProperties(itemTypes, excludedList, skipItemTypeProperties);
}

private class ItemType
{
	public string ItemTypeId { get; set; }
	public List<string> PropertiesList = new List<string>(); 
}

private class Excluded : ItemType
{}

private Item GetItemTypeWithProperties(List<ItemType> itemTypes, List<Excluded> excludedList, bool skipItemTypeProperties = false)
{
	var aml = "";
	var template = "<Item type=\"ItemType\" id=\"{0}\" action=\"get\"><Relationships><Item type=\"Property\" action=\"get\" select=\"config_id, data_source, data_type, is_multi_valued, keyed_name, label, readonly, name, is_hidden\" where=\"[property].data_type != 'md5'" 
		+ (skipItemTypeProperties ? " AND [property].data_type != 'item'" : "") + "\">{1}{2}</Item></Relationships></Item>";
	foreach (var item in itemTypes)
	{
		var nameTag = "";
		var ids = "";
		var names = string.Join(",", (item.PropertiesList.Count != 0 ? item.PropertiesList.Select(x => "'" + x + "'") : new List<string>()));

		var excluded = excludedList.FirstOrDefault(x => string.Compare(x.ItemTypeId, item.ItemTypeId, StringComparison.OrdinalIgnoreCase) == 0);
		if (excluded != null && excluded.PropertiesList.Count != 0)
		{
			ids = "<not><id condition=\"in\">" + string.Join(",", excluded.PropertiesList) + "</id></not>";
		}
		if (!string.IsNullOrEmpty(names))
		{
			nameTag = "<name condition=\"in\">" + names +"</name>";
		}
		
		aml += string.Format(template, item.ItemTypeId, ids, nameTag);
	}

	return this.getInnovator().applyAML("<AML>" + aml + "</AML>");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetItemTypeProperties' and [Method].is_current='1'">
<config_id>3D213E1ECCC9471487EDDF17731B695C</config_id>
<name>GetItemTypeProperties</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
