/*
目的:判斷報價單結構是否正確
傳入參數:this
做法:
1.
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

Item itmR = null;
Item itmSQL = null;
string aml = "";
string sql = "";

string strQuoteId = this.getProperty("id","");

Item itmQuote =this;

//從下往上算,第三階累加,碰到第二階就累加並且歸零
aml = "<AML>";
aml += "<Item type='in_Quote_Details' action='get' orderBy='sort_order DESC' >";
aml += "<source_id>" + strQuoteId + "</source_id>";
aml += "</Item></AML>";
Item itmQuote_Details = inn.applyAML(aml);

string strPrevLev="";

for(int k=0;k<itmQuote_Details.getItemCount();k++)
{
	Item itmQuote_Detail = itmQuote_Details.getItemByIndex(k);
	string strLev = itmQuote_Detail.getProperty("in_level","0");
	switch(strLev)
	{
		case "1":
			//由下而上累計,第一階的前一個必須是第二階
 			if(strPrevLev!="2")
 			throw new Exception("報價階層錯誤:#" + itmQuote_Detail.getProperty("sort_order","0") + "-->第一階下面必須是第二階報價項");
			break;
		case "2":
			//由下而上累計,第二階的前一個必須是第3階
 			if(strPrevLev!="3")
 				throw new Exception("報價階層錯誤:#" + itmQuote_Detail.getProperty("sort_order","0") + "-->第二階下面必須是第三階報價項");
			break;
		case "3":
			break;
		default:
 			throw new Exception("報價階層錯誤:#" + itmQuote_Detail.getProperty("sort_order","0") + "-->此報價項階層為" + strLev);

			break;
	}
	strPrevLev = strLev;
}



return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CheckQuoteLevel' and [Method].is_current='1'">
<config_id>4CAEE400028D4DEEBA18270F21B9AEC6</config_id>
<name>In_CheckQuoteLevel</name>
<comments>判斷報價單結構是否正確</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
