//System.Diagnostics.Debugger.Break();
/*
目的:專案Promote的時候,要執行事項
位置:Project Lifecycle PostMethod
說明:

*/

Innovator inn = this.getInnovator();
string aml = "";
string error= "";
string state = this.getProperty("state","");
Item itmWorkOrder = null;

switch(state)
{
	case "Active":
		this.apply("In_UpdateAct2ProjInfo");
    	if(this.getProperty("in_isbudget","")=="1")
		{
			this.apply("In_PLM2ERP");
		}
		
		break;
	case "Pending":
		break;
	case "Closed":
	case "Cancelled":
		break;
	default:
		break;
		
}

if(error!="")
	throw new Exception(error);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Project_PostPromote' and [Method].is_current='1'">
<config_id>B4BCA85919244963BA2421F20A42B478</config_id>
<name>In_Project_PostPromote</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
