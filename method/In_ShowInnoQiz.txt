//In_ShowInnoQiz
//顯示日期調查表的調查結果

var param = {aras:top.aras,itemtypeName:'in_Qiz',multiselect:1};
var res = showModalDialog('../scripts/searchDialog.html', param, 'dialogHeight:450px; dialogWidth:700px; status:0; help:0; resizable:1');
if (res === undefined || res.itemID ==='') {return false;}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ShowInnoQiz' and [Method].is_current='1'">
<config_id>3CA3F4A2DD3A4912AC3778EBF1DA92FE</config_id>
<name>In_ShowInnoQiz</name>
<comments>Edit By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
