Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strType = "In_TimeRecord";
string strMethod = "In_SetManager";

aml = "<AML>";
aml += "<Item type='" + strType + "' action='get'>";
aml += "</Item></AML>";

Item itmItems = inn.applyAML(aml);
for(int i=0;i<itmItems.getItemCount();i++)
{
	Item itmItem =  itmItems.getItemByIndex(i);
	itmItem.apply(strMethod);
}

Item itmR = this;

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetManager_All' and [Method].is_current='1'">
<config_id>73965813560F4235B4EF905D4494B797</config_id>
<name>In_SetManager_All</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
