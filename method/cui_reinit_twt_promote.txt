if (inArgs.isReinit) {
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calc_tearoff_states');
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}
	return {'cui_disabled': !inArgs.eventState.isPromote};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_twt_promote' and [Method].is_current='1'">
<config_id>558B9250A2804FECAF642664BDA0B019</config_id>
<name>cui_reinit_twt_promote</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
