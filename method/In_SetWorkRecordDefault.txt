//  can be used on any Item Type with the 
// "OnAfterNew" Event (Client Events tab)
// will set the owned_by_id to the current user's id (creator)
//debugger;
if ("" === this.getProperty("owned_by_id",""))
{
  var aliasId = this.getInnovator().getUserAliases();
  if (aliasId)
  {
    aliasId = aliasId.substr(0,32);
    var alias = this.getInnovator().getItemById("Identity", aliasId);
    if (alias.getItemCount()==1)
    {
      this.setProperty("owned_by_id", alias.getAttribute("id"));
      this.setPropertyAttribute("owned_by_id", "keyed_name", alias.getProperty("keyed_name", ""));

      //this.setProperty("manager1",  alias.getProperty("manager1"));
      //this.setProperty("manager2",  alias.getProperty("manager2"));
     
    }
  }
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetWorkRecordDefault' and [Method].is_current='1'">
<config_id>12E74AE31809412695F4DDC4634345C4</config_id>
<name>In_SetWorkRecordDefault</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
