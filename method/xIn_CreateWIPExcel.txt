string strMethodName = "In_CreateWIPExcel";

//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";

string strPos = "";
string PSCCxml = "";
string strValue ="";
string strDateTime = "";
string strNewValue1 = "";
string strNewValue2 = "";
string strPropertyName = "";
string strkeyName = "keyed_name";

int intR = 0;
int intOrder = 0;
int intCount = 0;
int intTemplate = 0;

double dblNum1 = 0;
double dblNum2 = 0;
double dblNum3 = 0;
double dblNum4 = 0;

Item itmR = this;
Item itmRelationship = null;

string[] arrRelationship = {"In_budget_Income_Aggregated","In_Budget_Staff_Budget","In_Budget_Aggregated","In_Budget_Aggregated","In_Budget_Aggregated"};

string[] arrBudgetIncomeAggregateds = {"in_accounting","in_f_actcost_m","in_actcost"};

string[] arrBudgetStaffBudgets = {"in_rank","in_timerecord_real_m","in_real_m","in_timerecord_real","in_real"};

string[] arrBudgetAggregateds = {"in_accounting","in_f_actcost","in_actcost"};

string[] strMonth = {"無","一","二","三","四","五","六","七","八","九","十","十一","十二"};

string[] arrProperties = arrBudgetIncomeAggregateds;

List<string> ListSort = new List<string>();

string strAccountingItemIDs = "439DAFFE525545049F53ABE4D5181A76,B4769D9CA387407FA5C4F126A92685B7,FDC62F15D6694315A477A9A31E3FF554,29951B09A1094D5FBCA23D976537DA86,7D2B0FF2E5454994ADEDD357E1B53FC2,94FBD0DBACFC4F669398A376CF5DA6F5,1E643315CC2A41C5831B966C63337B0E67DAB244EED844829DB5372FC134FFB6,AD7AB070A05C40DEA4736EA6B8021295";

string strStartDay = "21";
string strEndDay = "21";
//若沒指定 yearmonth 假屬性,則以執行的當月份為計算基礎
string strYearMonth  =  this.getProperty("yearmonth",System.DateTime.Now.ToString("yyyy-MM"));
strYearMonth = "2017-02";
string strEndDate = strYearMonth + "-" + strEndDay + "T00:00:00";
DateTime dtEndDate = Convert.ToDateTime(strEndDate);
string strStartDate = dtEndDate.AddMonths(-1).ToString("yyyy-MM") + "-" + strStartDay + "T00:00:00";
DateTime dtStartDate = Convert.ToDateTime(strStartDate);
string strLastDate = Convert.ToDateTime(dtEndDate.AddMonths(1).ToString("yyyy-MM") + "-01T00:00:00").AddDays(-1).ToString("yyyy-MM-dd") + "T00:00:00";

int intMonth = Convert.ToInt32(dtEndDate.ToString("MM"));

try
{
    //1.產生報表
    strDateTime = strMonth[intMonth] + "月" + strEndDate.Split('-')[1] + "," + strEndDate.Split('-')[0] + "---十二月31," + strEndDate.Split('-')[0];
    
    PSCCxml = "<in_param>";
    PSCCxml += "<prefix>^</prefix>";
    PSCCxml += "<sheet name='WIP'>";
    PSCCxml += "<body_variable>";
    PSCCxml += "<variable name='in_datetime' datatype='' >" + strDateTime + "</variable>";
    PSCCxml += "<variable name='in_company' datatype='' >" + this.getProperty("in_company","") + "</variable>";
    PSCCxml += "</body_variable>";
    PSCCxml += "<repeat_variable>";
    
    for(int k=0;k<arrRelationship.Length;k++){
        
        intOrder = 0;
        intCount = 0;
        intTemplate ++;
        strNewValue2 = "";
        ListSort = new List<string>();
        
        //2.取得表身
        aml = "<AML>";
        aml += "<Item type='" + arrRelationship[k] + "' action='get'>";
        aml += "<source_id>" + this.getID() + "</source_id>";
        
        if(k==1){
            aml += "<in_rank><Item type='In_Rank' action='get'><keyed_name condition='like'>"+this.getProperty("in_company","")+"*</keyed_name></Item></in_rank>";
        }
        
        if(k==2){
            aml += "<in_accounting><Item type='In_Accounting_items' action='get'><in_accounting condition='not like'>6210*</in_accounting></Item></in_accounting>";
            aml += "<in_accounting><Item type='In_Accounting_items' action='get'><in_accounting condition='not like'>7*</in_accounting></Item></in_accounting>";
        }
        
        if(k==3){
            aml += "<in_accounting><Item type='In_Accounting_items' action='get'><in_accounting condition='not like'>7*</in_accounting></Item></in_accounting>";
        }
        
        if(k==4){
            aml += "<in_accounting><Item type='In_Accounting_items' action='get'><in_accounting condition='like'>7*</in_accounting></Item></in_accounting>";
        }
        
        aml += "<in_accounting><Item type='In_Accounting_items' action='get'><in_company>"+this.getProperty("in_company","")+"</in_company></Item></in_accounting>";
        aml += "</Item></AML>";
        Item itmRelationships = inn.applyAML(aml);
        
        if(k==1){
            strPropertyName = "in_rank";
        }else{
            strPropertyName = "in_accounting";
        }
        
        intR = itmRelationships.getItemCount();
        intR --;
        
        for(int j=0;j<itmRelationships.getItemCount();j++){
            Item itmSort = itmRelationships.getItemByIndex(j);
            string strSort = itmSort.getPropertyAttribute(strPropertyName,strkeyName,"");
            strSort += "_" + itmSort.getID();
            ListSort.Add(strSort);
        }
        ListSort.Sort();
        
        for(int l=0;l<itmRelationships.getItemCount();l++){
            itmRelationship = inn.getItemById(arrRelationship[k],ListSort[l].Split('_')[1]);
            strNewValue1 = ListSort[l].Split('_')[0];
            
            if(k>1){
                if(strAccountingItemIDs.Contains(itmRelationship.getProperty("in_accounting",""))){
                    continue;
                }
            }
            
            if(k==1){
                if(strNewValue2 != ""){
                    if(strNewValue2.Split('-')[1] != strNewValue1.Split('-')[1]){
                        intOrder ++;
                        if(intCount==0){
                            strPos = "fix";
                        }else{
                            strPos = "continue";
                        }
                        intCount ++;
                        
                        PSCCxml += "<row template='" + intTemplate.ToString() + "' order='" + intOrder.ToString() + "' pos='" + strPos + "'>";
                        PSCCxml += "<variable name='" + arrProperties[0] + "' datatype='' >" + strNewValue2.Split('-')[1] + "</variable>";
                        PSCCxml += "<variable name='" + arrProperties[1] + "' datatype='' >" + dblNum1.ToString() + "</variable>";
                        PSCCxml += "<variable name='" + arrProperties[2] + "' datatype='' >" + dblNum2.ToString() + "</variable>";
                        PSCCxml += "<variable name='" + arrProperties[3] + "' datatype='' >" + dblNum2.ToString() + "</variable>";
                        PSCCxml += "<variable name='" + arrProperties[4] + "' datatype='' >" + dblNum2.ToString() + "</variable>";
                        PSCCxml += "</row>";
                        
                        dblNum1 = 0;
                        dblNum2 = 0;
                        dblNum3 = 0;
                        dblNum4 = 0;
                    }
                }
            }else{
                intOrder ++;
                if(intCount==0){
                    strPos = "fix";
                }else{
                    strPos = "continue";
                }
                intCount ++;
            }
            
            if(k!=1){
                PSCCxml += "<row template='" + intTemplate.ToString() + "' order='" + intOrder.ToString() + "' pos='" + strPos + "'>";
            }
            
            if(k==1){
                arrProperties = arrBudgetStaffBudgets;
            }
            
            if(k>=2){
                arrProperties = arrBudgetAggregateds;
            }
            
            for(int m=0;m<arrProperties.Length;m++){
                
                strValue = itmRelationship.getPropertyAttribute(arrProperties[m],"keyed_name","");
                
                if(strValue==""){
                    
                    if(arrProperties[m] == "in_timerecord_real_m" || arrProperties[m] == "in_real_m" || arrProperties[m] == "in_f_actcost_m"){
                        strValue = arrProperties[m];
                        strValue = strValue.Replace("_m","_" + intMonth);
                        strValue = itmRelationship.getProperty(strValue,"");
                        
                        if(k==1){
                            if(strNewValue2 == ListSort[l].Split('_')[0]){
                                if(m==1){
                                    dblNum1 += Convert.ToDouble(strValue);
                                }
                                if(m==2){
                                    dblNum2 += Convert.ToDouble(strValue);
                                }
                            }
                        }else{
                            PSCCxml += "<variable name='" + arrProperties[m] + "' datatype='' >" + strValue + "</variable>";
                        }
                    }else{
                        strValue = itmRelationship.getProperty(arrProperties[m],"");
                        if(k == 1){
                            if(strNewValue2 == ListSort[l].Split('_')[0]){
                                if(m==3){
                                    dblNum3 += Convert.ToDouble(strValue);
                                }
                                if(m==4){
                                    dblNum4 += Convert.ToDouble(strValue);
                                }
                            }
                        }
                        
                        if(k==2){
                            aml = "<AML>";
                            aml += "<Item type='"+arrRelationship[k]+"' action='get'>";
                            aml += "<source_id>" + this.getID() + "</source_id>";
                            aml += "<in_accounting>" + itmRelationship.getProperty("in_accounting","") + "</in_accounting>";
                            aml += "</Item></AML>";
                            Item itmBudgetAggregated = inn.applyAML(aml);
                            
                            if(!itmBudgetAggregated.isError()){
                                
                                string strValue1 = itmBudgetAggregated.getProperty(arrProperties[m]);
                                
                                if(strValue == "" || strValue == null){
                                    strValue = "0";
                                }
                                
                                if(strValue1 == "" || strValue1 == null){
                                    strValue1 = "0";
                                }
                                
                                int intValue1 = Convert.ToInt32(strValue);
                                int intValue2 = Convert.ToInt32(strValue1);
                                int intNum = intValue1 - intValue2;
                                strValue = intNum.ToString();
                            }else{
                                continue;
                            }
                        }
                        if(k!=1){
                            PSCCxml += "<variable name='" + arrProperties[m] + "' datatype='' >" + strValue + "</variable>";
                        }
                    }
                }else{
                    if(k==1){
                        if(strNewValue2 == ""){
                            strNewValue2 = strValue;
                        }else{
                            if(strNewValue2 != ListSort[l].Split('_')[0]){
                                strNewValue2 = strValue;
                            }
                        }
                    }else{
                        PSCCxml += "<variable name='" + arrProperties[m] + "' datatype='' >" + strValue + "</variable>";
                    }
                }
            }
            
            if(k!=1){
                PSCCxml += "</row>";
            }
            
            if(k==1){
                if(intR == l){
                    intOrder ++;
                    if(intCount==0){
                        strPos = "fix";
                    }else{
                        strPos = "continue";
                    }
                    
                    intCount ++;
                    
                    PSCCxml += "<row template='" + intTemplate.ToString() + "' order='" + intOrder.ToString() + "' pos='" + strPos + "'>";
                    PSCCxml += "<variable name='" + arrProperties[0] + "' datatype='' >" + strNewValue2.Split('-')[1] + "</variable>";
                    PSCCxml += "<variable name='" + arrProperties[1] + "' datatype='' >" + dblNum1.ToString() + "</variable>";
                    PSCCxml += "<variable name='" + arrProperties[2] + "' datatype='' >" + dblNum2.ToString() + "</variable>";
                    PSCCxml += "<variable name='" + arrProperties[3] + "' datatype='' >" + dblNum2.ToString() + "</variable>";
                    PSCCxml += "<variable name='" + arrProperties[4] + "' datatype='' >" + dblNum2.ToString() + "</variable>";
                    PSCCxml += "</row>";
                    
                    dblNum1 = 0;
                    dblNum2 = 0;
                    dblNum3 = 0;
                    dblNum4 = 0;
                }
            }
        }
    }
    PSCCxml += "</repeat_variable>";
    PSCCxml += "</sheet>";

    PSCCxml += "<source_file_aml>";
    PSCCxml += "<Item type='In_Variable' action='get' select='in_file'>";
    PSCCxml += "<in_name>Template_WIP-Company</in_name>";
    PSCCxml += "</Item>";
    PSCCxml += "</source_file_aml>";

    PSCCxml += "<source_property>in_file</source_property>";

    PSCCxml += "<return_itemtype>In_Budget</return_itemtype>";
    PSCCxml += "<return_id>" + this.getID() + "</return_id>";

    PSCCxml += "<return_filename>WIP-Project" + "-" + this.getProperty("in_company","") + "-" + this.getProperty("in_year","") + "-" + intMonth + ".xlsx</return_filename>";

    PSCCxml += "<return_item type='aml'>";
    PSCCxml += "<Item type='In_Budget_Report' action='add'>";
    PSCCxml += "<source_id>" + this.getID() + "</source_id>";
    PSCCxml += "<related_id>@fileid</related_id>";
    PSCCxml += "</Item></return_item>";
    
    PSCCxml += "</in_param>";
    
    Item itmQueue = _InnH.CreateQueue(this,"xml_to_excel",PSCCxml,"");
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xIn_CreateWIPExcel' and [Method].is_current='1'">
<config_id>FB9351EB78D54F669245A9A09752F42B</config_id>
<name>xIn_CreateWIPExcel</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
