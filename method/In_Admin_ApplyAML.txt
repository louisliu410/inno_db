var fld_AML = getFieldByName("AML");
var elements = fld_AML.getElementsByTagName("textarea");
var aml = elements[0].value;

var inn = top.aras;

var action_name = getFieldByName("ddl_action").getElementsByTagName("select")[0].value;
var r;

if(action_name=="")
{
    alert("請選擇Action");
    return;
}

if(action_name=="AML")
    r = inn.applyAML(aml);
    
if(action_name=="SQL")
    r = inn.applyMethod("In_ApplySQL",aml);

if(action_name=="Item")
{
    aml = "<AML>" + aml + "</AML>";
    r = inn.applyAML(aml);
}
    
    
var fld_AMLResult = getFieldByName("AMLResult");
elements = fld_AMLResult.getElementsByTagName("textarea");
elements[0].value = r;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Admin_ApplyAML' and [Method].is_current='1'">
<config_id>DF30204A86F24B6DB6CA71CD6459A9ED</config_id>
<name>In_Admin_ApplyAML</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
