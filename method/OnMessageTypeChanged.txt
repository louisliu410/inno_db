var itm = document.item;
if (!itm) {
	return;
}

var messageType = aras.getItemProperty(itm, 'is_standard_template');
var customHtmlElem = getFieldByName('custom_html');
var widthElem = getFieldByName('width');
var heightElem = getFieldByName('height');
var cssElem = getFieldByName('css');

if ('1' == messageType) {
	if (customHtmlElem) {
		customHtmlElem.style.visibility = 'hidden';
	}

	if (widthElem) {
		widthElem.style.display = 'none';
	}

	if (heightElem) {
		heightElem.style.display = 'none';
	}

	if (cssElem) {
		cssElem.style.display = 'block';
	}
} else {
	if (widthElem) {
		widthElem.style.display = 'block';
	}

	if (heightElem) {
		heightElem.style.display = 'block';
	}

	if (cssElem) {
		cssElem.style.display = 'none';
	}

	if (customHtmlElem) {
		customHtmlElem.style.visibility = '';
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnMessageTypeChanged' and [Method].is_current='1'">
<config_id>5A3E39F239A849D4A31D4B5F66EC8F55</config_id>
<name>OnMessageTypeChanged</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
