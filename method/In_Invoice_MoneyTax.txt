/*
目的:
做法:
1.檢查表單是否上鎖
In_Invoice_detail_2
位置:物件動作
*/

var inn = this.getInnovator();
var rel_name = "In_Invoice_detail_2";

//1.檢查表單是否上鎖
if(this.getLockStatus() !== 0)
{
    
	alert(inn.applyMethod("In_Translate","<text>請先解鎖本文件再執行</text>").getResult());
	return;
}

if(this.getProperty("in_contract","")=="")
{
    rel_name = "In_Invoice_Details";
}

var param =  "<parent_type>"+thisItem.getType()+"</parent_type>";
    param += "<parent_id>"+thisItem.getID()+"</parent_id>";
    param += "<rel_type>"+rel_name+"</rel_type>";
    param += "<money_tax>in_payment_o</money_tax><money>in_invoice_o</money><tax>in_tax_o</tax>";
    param += "<base>money_tax</base>";
    
var itmApplyMethod = inn.applyMethod("In_MoneyTaxHandler",param);

this.apply("In_PaymentCompute");

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Invoice_MoneyTax' and [Method].is_current='1'">
<config_id>2FCC262B3AD641A0B849F4BCEB8716BF</config_id>
<name>In_Invoice_MoneyTax</name>
<comments>公用稅額(業主請款單)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
