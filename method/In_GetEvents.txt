/*
目的:取得符合條件的 In_Events
做法:
1.將符合 Criteria 得 In_Events 回傳
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
string aml = "";
string str_r="";
Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

try
{
	string strProperty = Innosoft.InnUtility.GetParameterValue(criteria,"property",'&','=',"");
	string strWhos = Innosoft.InnUtility.GetParameterValue(criteria,"whos",'&','=',"");
	string strIds = Innosoft.InnUtility.GetParameterValue(criteria,"ids",'&','=',"");
	//string strStart = Innosoft.InnUtility.GetParameterValue(criteria,"start",'&','=',""); //年-月
	//string strEnd = Innosoft.InnUtility.GetParameterValue(criteria,"end",'&','=',""); //年-月
	string strMonth = Innosoft.InnUtility.GetParameterValue(criteria,"month",'&','=',""); //年-月
	string strMode = Innosoft.InnUtility.GetParameterValue(criteria,"mode",'&','=',"1");  //1:拆成每一個單天,2:保留時間區間
	string strEventItemType = Innosoft.InnUtility.GetParameterValue(criteria,"event_itemtype",'&','=',"In_Events"); 

	//strCriterias = strCriterias.Trim(',');
	//strCriterias = "'" + strCriterias.Replace(",","','") + "'";

	strIds = strIds.Trim(',');
	strIds = "'" + strIds.Replace(",","','") + "'";
	
	string strUniqueKeyedName = "";
	
	if(strMonth=="")
		strMonth = System.DateTime.Now.ToString("yyyy-MM");
	string strStart = strMonth + "-01T00:00:00";
	DateTime dtEnd = Convert.ToDateTime(strMonth).AddMonths(1);
	
	aml = "<AML>";
	aml += "<Item type='" + strEventItemType + "' action='get' select='keyed_name,in_event_title,in_event_startdate,in_event_enddate,in_event_who,in_event_thing,in_event_location,in_event_description'>";
	aml += "<in_event_startdate condition='gt'>" + strStart + "</in_event_startdate>";
	aml += "<in_event_enddate  condition='lt'>" + dtEnd.ToString("yyyy-MM-ddTHH:mm:ss") + "</in_event_enddate>";
	switch(strProperty)
	{
		case "in_event_who":
			aml += "<in_event_who condition='in'>";
			if(strWhos!=""){
				aml += "<Item type='Identity' action='get'>";
				aml += "<keyed_name condition='in'>" + strWhos + "</keyed_name>";
				aml += "</Item>";
			}
			else
			{
				aml += strIds;
			}			
			aml += "</in_event_who>";
			break;
		case "in_event_thing":		
			break;
		default:
			break;
	}	
	aml += "</Item></AML>";
	Item itmEvents = inn.applyAML(aml);
	
	//將事件轉換成個別日子
	
	Item itmSeperateEvents = inn.newItem("In_Events");
	/*
	for(int i=0;i<itmEvents.getItemCount();i++)
	{
		Item itmEvent = itmEvents.getItemByIndex(i);
		strUniqueKeyedName = Innosoft.InnUtility.PushUniqueQueue(strUniqueKeyedName,itmEvent.getPropertyAttribute(strProperty,"keyed_name"),",");
		DateTime dtEventFrom = Convert.ToDateTime(itmEvent.getProperty("in_event_startdate"));
		DateTime dtEventTo = Convert.ToDateTime(itmEvent.getProperty("in_event_enddate"));
		TimeSpan tsDiff =dtEventTo - dtEventFrom;
		int intDiffDays = tsDiff.Days;
		itmEvent.setProperty("who",itmEvent.getPropertyAttribute(strProperty,"keyed_name"));
		if(intDiffDays==0)
		{
			itmSeperateEvents.appendItem(itmEvent);
		}
		else
		{
			for(int k=0;k<=intDiffDays;k++)
			{
				itmEvent.setProperty("in_event_startdate",dtEventFrom.AddDays(k).ToString("yyyy-MM-ddTHH:mm:ss"));
				itmEvent.setProperty("in_event_enddate",dtEventFrom.AddDays(k).ToString("yyyy-MM-ddTHH:mm:ss"));
				itmSeperateEvents.appendItem(itmEvent);
			}
		}
	}
	*/
	for(int i=0;i<itmEvents.getItemCount();i++)
	{
		Item itmEvent = itmEvents.getItemByIndex(i);
		strUniqueKeyedName = Innosoft.InnUtility.PushUniqueQueue(strUniqueKeyedName,itmEvent.getPropertyAttribute(strProperty,"keyed_name"),",");
		itmEvent.setProperty("who",itmEvent.getPropertyAttribute(strProperty,"keyed_name"));
	}
	
	strUniqueKeyedName = strUniqueKeyedName.Trim(',');
	if(!itmEvents.isError())
	{
		//str_r=itmSeperateEvents.dom.SelectSingleNode("//AML").InnerXml;
		str_r=itmEvents.dom.SelectSingleNode("//Result").InnerXml;
	}
	
	
	str_r += "<group_who>";
	string[] arrWhos=strWhos.Split(',');
	for(int i=0;i<arrWhos.Length;i++)
	{
		str_r += arrWhos[i].Trim('\'') + ",";
	}
	str_r = str_r.Trim(',');
	str_r += "</group_who>";
	str_r += "<month>" + strStart.Replace("-01T00:00:00","") + "</month>";
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("true",str_r,"",criteria);
}
catch (Exception ex)
{
    criteria = "<request><![CDATA[" + criteria + "]]></request>";
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}

return  inn.newResult(r);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetEvents' and [Method].is_current='1'">
<config_id>17C826766E4A44A9B5EF74F68F4896FE</config_id>
<name>In_GetEvents</name>
<comments>inn core app</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
