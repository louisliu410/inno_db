/*
目的:依據HR匯入的薪資表計算出個人時薪
做法:
1.接收XML
2.XML內的薪資數字為已經計算好含勞退與年終的薪資
3.將計算後結果更新至user的cost欄位內,允許 匯入薪資為0

<Item type="User" action="merge" where="[user_no]='BT151001'">
        <user_no>BT151001</user_no>
        <in_year>2016</in_year>
        <in_month>05</in_month>
        <in_money>46564</in_money>
        <workhours>176</workhours>
</Item>

*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.InnoERP _InnERP = new Innosoft.InnoERP(inn);
string aml = "";
string sql = "";
Item itmR = null;
string strErrorMessage = "";
string strCompany = "";
try
{

    XmlNodeList xnUsers = this.dom.SelectNodes("//salary/Item");
	string strYear = this.getProperty("in_year","");
	string strMonth = this.getProperty("in_month","");
	string strYearMonth = strYear + "-" + strMonth;
    strErrorMessage = "";
    foreach (XmlNode xnUser in xnUsers)
    {
        if( xnUser.SelectSingleNode("./user_no") ==null)           
            continue;

		Item itmUser = inn.newItem("User");
		itmUser.loadAML(xnUser.OuterXml);
		string strMoney =  xnUser.SelectSingleNode("./in_money").InnerText;
        double dblCost = Innosoft.InnUtility.GetNumber(strMoney,0);   
        double dblCostPerHour=dblCost;

        string strUserNo = xnUser.SelectSingleNode("./user_no").InnerText;
		//if(dblCost==0){			
		//	throw new Exception("[" + strUserNo  + "]無薪資資料");
		//}
		/*if(dblWorkHours==0){			
			throw new Exception("[" + strUserNo  + "]無工時資料");
		}*/
		//dblCostPerHour = Math.Round(dblCost/dblWorkHours, MidpointRounding.AwayFromZero);


		strYear = itmUser.getProperty("in_year","");
		strMonth = itmUser.getProperty("in_month","");
		strYearMonth = strYear + "-" + strMonth;

		sql = "Update [User] set in_cost=" + dblCostPerHour.ToString() + " where user_no='" + strUserNo + "'";
		inn.applySQL(sql);
		Innosoft.InnUtility.AddLog("已更新時薪:" + strUserNo,"MergeUserCost_" + strYearMonth);

		if(strCompany=="")
		{
			aml = "<AML>";
			aml += "<Item type='User' action='get' where=\"[User].[user_no]='" + strUserNo + "'\">";			
			aml += "</Item></AML>";
			itmUser = inn.applyAML(aml);
			strCompany = itmUser.getProperty("in_company","");
		}
    }




}
catch(Exception ex)
{   
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

    string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
    string strErrorDetail="";
    if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
    {
        strError = "匯入主檔失敗:無法執行AML:" + aml ;
    }
	//if(strErrorMessage!="")
	//	strError = strErrorMessage + itmR.getErrorString();
    strErrorDetail = strError + "\n" + ex.ToString() + ":" +  aml ;
    Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
    return inn.newError(strError);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_MergeUserCost' and [Method].is_current='1'">
<config_id>8E25288BE67343D5AF86A38B94DEB3BE</config_id>
<name>In_MergeUserCost</name>
<comments>依據HR匯入的薪資表計算出個人時薪</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
