
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);



if(this.isNew() || this.getAttribute("isTemp", "").Equals("1", StringComparison.OrdinalIgnoreCase))
{
  throw new Exception(_InnH.Translate("請先儲存並解鎖本文件再執行"));
}

if(this.getLockStatus() != 0)
{
    throw new Exception(_InnH.Translate("請先解鎖本文件再執行"));
}

string strLogIns = Aras.Server.Security.Permissions.Current.IdentitiesList;

string aml = "";
string sql = "";
string strState = "";
string strID = "";
int intNum = 0;
int intVersion = 0;

Item resItem = null;

aml ="<AML>";
aml += "<Item type='"+this.getType()+"' action='get'>";
aml += "<id>" + this.getID() + "</id>";
aml += "</Item></AML>";
Item itmItemType = inn.applyAML(aml);

this.setAttribute("check_action","can_update"); //can_update, can_delete.....
Item itmCanAccess = this.apply("In_CanAccess"); //回傳 yes, no

if(itmCanAccess.getResult()!="yes")
{
    throw new Exception(_InnH.Translate("可鎖定的使用者才能建立新版本"));
}

switch(this.getType())
{
    case "Document":

    if(this.getProperty("state","") != "Released")
    {
        throw new Exception(_InnH.Translate("狀態必須為[Released]才能執行"));
    }

    resItem = this.apply("version");
    if(resItem.isError()){
        return resItem;
    }
    resItem = resItem.apply("unlock");

    sql = "Update Document set ";
    sql += "release_date = null,";
    sql += "effective_date = null,";

    strState = "Preliminary";

    break;

    case "in_proposal":

    if(this.getProperty("state","") != "In Process")
    {
        throw new Exception(_InnH.Translate("狀態必須為[In Process]才能執行"));
    }

    intVersion = Convert.ToInt32(this.getProperty("in_version",""));

    intVersion ++;

    sql = "Update in_proposal set ";
    sql += "in_version = '" + intVersion + "',";

    strState = "Start";

    

    resItem = this;

    break;

    case "In_Budget":

    if(this.getProperty("state","") != "Active")
    {
        throw new Exception(_InnH.Translate("狀態必須為[Active]才能執行"));
    }

    resItem = this.apply("version");
    if(resItem.isError()){
        return resItem;
    }
    resItem = resItem.apply("unlock");

    sql = "Update In_Budget set ";
    sql += "release_date = null,";
    sql += "effective_date = null,";

    strState = "Preliminary";

    break;

    case "in_contract":

    if(this.getProperty("state","") != "Active"){
        return inn.newError("狀態必須為[Active]才能執行");
    }

    resItem = this.apply("version");
    if(resItem.isError()){
        return resItem;
    }
    resItem = resItem.apply("unlock");

    sql = "Update in_contract set ";
    sql += "release_date = null,";
    sql += "effective_date = null,";

    strState = "Preliminary";

    break;

    default:
    sql = "Update ["+this.getType()+"] set ";
    break;
}

sql += "in_wfp_state = null,";
sql += "in_current_activity = null";
sql += " where id = '" + resItem.getID() + "'";
inn.applySQL(sql);

//必須在這邊才轉換身分,否則會造成前面的權限驗證出問題
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
Aras.Server.Security.Identity plmIdentity1 = Aras.Server.Security.Identity.GetByName("Super User");

    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
	bool PermissionWasSet1 = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity1);


switch(this.getType())
{
	case "in_proposal":
		aml = "<AML>";
		aml += "<Item type='in_proposal_history' action='add'>";
		aml += "<source_id>"+this.getID()+"</source_id>";
		aml += "<name>"+this.getProperty("name","")+"</name>";
		aml += "<in_company>"+this.getProperty("in_company","")+"</in_company>";
		aml += "<in_dept>"+this.getProperty("in_dept","")+"</in_dept>";
		aml += "<in_customer>"+this.getProperty("in_customer","")+"</in_customer>";
		aml += "<in_pm>"+this.getProperty("in_pm","")+"</in_pm>";
		aml += "<in_amount>"+this.getProperty("in_amount","")+"</in_amount>";
		aml += "<in_date_s>"+this.getProperty("in_date_s","")+"</in_date_s>";
		aml += "<in_date_e>"+this.getProperty("in_date_e","")+"</in_date_e>";
		aml += "</Item></AML>";
		Item itmAdd = inn.applyAML(aml);	
		break;
	default:
		break;
}

if(resItem.getProperty("state","")!=strState)
{
    resItem.setProperty("state",strState);
    Item itmProposal = resItem.apply("promoteItem");
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
if (PermissionWasSet1) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity1);
return resItem;




#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateVersion' and [Method].is_current='1'">
<config_id>E66EDD3132874CEBA94B342878B20FBB</config_id>
<name>In_UpdateVersion</name>
<comments>可鎖定的使用者即可改版</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
