/*
目的:將Vendor(廠商)的欄位帶到in_contract(合約管理)的欄位
使用In_CopyProsValueFromProItem
*/

var FromPropertyName = "in_vendor";
var PropertyMappings = "in_tax_code,in_tax_code;";
document.In_CopyProsValueFromProItem(FromPropertyName,PropertyMappings,"Y");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Copy_Vendor' and [Method].is_current='1'">
<config_id>A8F6347902534FC8A8F6C65357C8AC3A</config_id>
<name>In_Copy_Vendor</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
