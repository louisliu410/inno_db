grid.setNewItemCreationEnabled(false);
grid.overrideMethod("lockItemBeforeEditStart", "return true;");

var nds = this.dom.documentElement.selectNodes("Item[@type='Activity2 Task']");
var nd;
var v;
for (var i=0; i<nds.length; i++)
{
  nd = nds[i];
  v = (getItmProp(nd, "percent_compl")=="100") ? "1" : "0";
  setItmProp(nd, "fake_verified", v);
}

function setItmProp(itm, propNm, propVal)
{
  var nd = itm.selectSingleNode(propNm);
  if (!nd)
  {
    nd = itm.appendChild(itm.ownerDocument.createElement(propNm));
  }
  nd.text = propVal;
}

function getItmProp(itm, propNm, defaultVal)
{
  var retVal = defaultVal;
  var nd = itm.selectSingleNode(propNm);
  if (nd)
  {
    retVal = nd.text;
  }
  return retVal;
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_ACW_cfgTasksGridOnLoad' and [Method].is_current='1'">
<config_id>54E27AB24F954F80AED67D3EB23B0649</config_id>
<name>PM_ACW_cfgTasksGridOnLoad</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
