/*
目的:讓自動節點依據後面Workflow Process Path 上的 in_criteria上的 條件,自動判斷要走哪條路
位置:自動流程節點的onActive
做法:
1.抓到source_id為自己的所有 Workflow Process Path
2.將這些Path透過GetMatchCriteria Method 抓到符合條件的
3.將該path 的 is_default 都勾選為 true
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string param = "";
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Item ControlledItem = _InnH.GetInnoControlledItem(this);
if(ControlledItem.isError())
	return inn.newError(ControlledItem.getErrorString());

string strMatchedParents = _InnH.GetMatchedCriteriaItems(ControlledItem.getType(),  ControlledItem.getID(), ControlledItem.getAttribute("typeId",""), "Workflow Process Path",  "in_criteria", "[Workflow_Process_Path].[source_id]='" + this.getID() + "'", "OneResult");

//string strMatchedParents = itmTemp.getResult();
if(strMatchedParents=="")
	return inn.newError(_InnH.Translate("無對應的自動路徑"));

//先清空所有的自動路徑
string sql = "";
sql = "Update [Workflow_Process_Path] set is_default=0 where source_id='" + this.getID() + "'";
inn.applySQL(sql);

string strInParentId = strMatchedParents.Split(',')[0]; //取得符合條件的Criteria的第一筆
sql = "Update [Workflow_Process_Path] set is_default=1 where id='" + strInParentId + "'";
inn.applySQL(sql);

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AutoPathByCriteria' and [Method].is_current='1'">
<config_id>2D6AE7F0036440D99EE867A31F181D52</config_id>
<name>In_AutoPathByCriteria</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
