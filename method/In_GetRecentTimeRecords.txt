/*
目的:依據表頭上的使用者,查詢這個人近七日內的報工紀錄
位置:Grid的AML的Action
做法:
1.傳入In_PerlWorkRecord的id
2.取得 owner 的7天內的所有 in_timerecord

*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmR = null;
	
try
{
	string strId = this.getProperty("in_perlworkrecord");
	Item itmWorkRecord = inn.getItemById("In_PerlWorkRecord",strId);
	string strOwnerId = itmWorkRecord.getProperty("owned_by_id","");
	
	aml = "<AML>";
	aml += "<Item type='In_TimeRecord' action='get'>";
	aml += "<in_start_time condition='ge'>" + System.DateTime.Now.AddDays(-7).ToString("yyyy-MM-ddT00:00:00") + "</in_start_time>";
	aml += "<owned_by_id>" + strOwnerId + "</owned_by_id>";
	aml += "</Item></AML>";
	
	itmR = inn.applyAML(aml);
	
	for(int i=0;i<itmR.getItemCount();i++)
	{
		Item itmTmp = itmR.getItemByIndex(i);
		itmTmp.setProperty("fake_project_name",itmTmp.getPropertyAttribute("in_project","keyed_name",""));
		itmTmp.setProperty("fake_date",itmTmp.getProperty("in_start_time","").Split('T')[0]);
		itmTmp.setProperty("fake_time",itmTmp.getProperty("in_start_time","").Split('T')[1]);
		itmTmp.setProperty("fake_timerecordtype",_InnH.GetListLabel("In_TimeRecordType",itmTmp.getProperty("in_timerecordtype",""),""));
		itmTmp.setProperty("fake_owned_by_id",itmTmp.getPropertyAttribute("owned_by_id","keyed_name",""));
		itmTmp.setProperty("fake_function_code",itmTmp.getPropertyAttribute("in_function_code","keyed_name",""));		
	
	}
	
	
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "無法執行AML:" + aml ;
	}
	strErrorDetail = strError + "\n" + ex.ToString() + aml  + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	return inn.newError(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetRecentTimeRecords' and [Method].is_current='1'">
<config_id>B8737B8448724612A8F3DE8B05F17994</config_id>
<name>In_GetRecentTimeRecords</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
