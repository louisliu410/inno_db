/*
目的:另存報價單時,重設新產生報價單的報價細項的In_Config_Id
位置:AfterCopy
*/
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
string sql = "Update [In_Quote] set item_number='',in_project=null where id='" + this.getID() + "'";

string aml ="<AML>";
aml += "<Item type='In_Quote_Details' action='get'>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";

Item itmChildren = inn.applyAML(aml);

for(int i=0;i<itmChildren.getItemCount();i++)
{
 string newID = inn.getNewID();
 Item itmChild = itmChildren.getItemByIndex(i);
 itmChild.setProperty("in_config_id",newID);
 itmChild.apply("edit");
}

Item itmR = inn.applySQL(sql);
itmR=this.apply("In_GetNextNumber");

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Quote_AfterCopy' and [Method].is_current='1'">
<config_id>7D70258008AC47DEA1BC1E8910989C19</config_id>
<name>In_Quote_AfterCopy</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
