// //System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
string aml = "";
string sql = "";
Item itmItemType = null;
try
{
    aml  = "<AML>";
    aml += "<Item type='ItemType' action='get'>";
    aml += "<name condition='not in'>'In_WF_lItem'</name>";
    aml += "<is_relationship condition='ne'>1</is_relationship>";
    aml += "<Relationships>";
    aml += "<Item type='Property' action='get'>";
    aml += "<label>目前流程關卡</label>";
    aml += "</Item>";
    aml += "</Relationships>";
    aml += "</Item></AML>";
    itmItemType = inn.applyAML(aml);

    for(int i=0;i<itmItemType.getItemCount();i++)
    {
        itmItemType.getItemByIndex(i).apply("In_EnableWFFForItem");
    }
}
catch(Exception ex)
{
    string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;

	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(strError);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AddReType_WFF' and [Method].is_current='1'">
<config_id>073AE05DAB6646F68B4B00B0240483DA</config_id>
<name>In_AddReType_WFF</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
