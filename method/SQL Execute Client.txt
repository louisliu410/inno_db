var result = this.apply('SQL EXECUTE');
if (result.isError()) {
	result = '<Error />';
} else {
	var xmlDocument = new DOMParser().parseFromString(result.dom.xml, 'text/xml');
	var serializer = new XMLSerializer();
	result = serializer.serializeToString(xmlDocument);
}

var r = formatXml(result);
r = htmlEscape(r);
return '<PRE>' + r + '</PRE>';

function formatXml(xml) {
	var formatted = '';
	var reg = new RegExp('(>)(<)(\/*)', 'g');
	xml = xml.replace(reg, '$1\r\n$2$3');
	var pad = 0;
	each(xml.split('\r\n'), function(index, node) {
		var indent = 0;
		if (node.match(/.+<\/\w[^>]*>$/)) {
			indent = 0;
		} else if (node.match(/^<\/\w/)) {
			if (pad !== 0) {
				pad -= 1;
			}
		} else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
			indent = 1;
		} else {
			indent = 0;
		}

		var padding = '';
		for (var i = 0; i < pad; i++) {
			padding += '\t';
		}

		formatted += padding + node + '\r\n';
		pad += indent;
	});

	return formatted;
}

function htmlEscape(str) {
	return str.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function each(object, callback) {
	var name;
	var i = 0;
	var length = object.length;

	if (length === undefined) {
		for (name in object) {
			if (callback.call(object[name], name, object[name]) === false) {
				break;
			}
		}
	} else {
		for (var value = object[0]; i < length && callback.call(value, i, value) !== false; value = object[++i]) {}
	}

	return object;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SQL Execute Client' and [Method].is_current='1'">
<config_id>DF0CD797325E4C059F837E7DDFD4ADD4</config_id>
<name>SQL Execute Client</name>
<comments>Client method for display of server method result</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
