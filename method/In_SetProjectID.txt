/*
目的：依據in_activity2欄位找到對應的 專案,放入 in_project 欄位中
作法：
1.將 in_activity2 的 in_proj_id 值 填入 in_project
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{	
	string strAct2Id = this.getProperty("in_activity2","");
	if(strAct2Id == "")
		return this;
	
	Item itmAct2 = inn.getItemById("Activity2",strAct2Id);
	
	string strProj_Id=itmAct2.getProperty("in_proj_id","");
	if(strProj_Id=="")
		return this;
	string sql = "Update [" + this.getType().Replace(" ","_") + "] set in_project='" + strProj_Id + "' where id='" + this.getID() + "'";
	Item itmR = inn.applySQL(sql);

}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetProjectID' and [Method].is_current='1'">
<config_id>66CD6E7C965D463B874BCDD64F3F6900</config_id>
<name>In_SetProjectID</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
