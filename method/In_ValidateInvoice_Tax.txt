/*
目的:未稅金額+稅額 必須等於 發票金額
位置:onAfterUpdate 或 流程節點上
適用Itemtype:廠商請款單與客戶請款單
做法:
1.未稅金額+稅額 必須等於 發票金額
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;
	
try
{
	Item ControlledItem = _InnH.GetInnoControlledItem(this);
	if(ControlledItem.isError())
		ControlledItem = this;


	double dblTax = Convert.ToDouble(ControlledItem.getProperty("in_tax_o","0"));
	double dblInvoice_Tax = Convert.ToDouble(ControlledItem.getProperty("in_invoice_tax_o","0"));
	double dblInvoice = Convert.ToDouble(ControlledItem.getProperty("in_invoice_o","0"));
	
	if(dblInvoice_Tax!=dblTax+dblInvoice)
		throw new Exception("含稅金額(" + dblInvoice_Tax.ToString() + ")不等於未稅金額(" + dblInvoice.ToString() + ")+稅額(" + dblTax.ToString() + ")");
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ValidateInvoice_Tax' and [Method].is_current='1'">
<config_id>75F52E60B2BE4DF483E7DFD897C01A40</config_id>
<name>In_ValidateInvoice_Tax</name>
<comments>未稅金額+稅額 必須等於 發票金額</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
