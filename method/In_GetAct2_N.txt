/*
目的:提供 voteAct2的內容
參數:act2id
做法:
*/
string strMethodName = "In_GetAct2_N";
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.app _InnoApp = new Innosoft.app(inn);

string aml = "";
string sql = "";
string strUserId = inn.getUserID();
string strIdentityId = inn.getUserAliases();

//_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;
string strAct2Id = this.getProperty("act2id","");
string lang = "zt";
try
{
	string process_by_identity="";
	string Current_Act2AssId="";
	string r= "";

	//先確認這個任務是否可以被這個USER簽
	aml = "<UserID>" + inn.getUserID() + "</UserID>";
	aml += "<ViewMode>Both</ViewMode>";
	aml += "<WorkflowTasksFlag>0</WorkflowTasksFlag>";
	aml += "<ProjectTasksFlag>1</ProjectTasksFlag>";
	aml += "<ActionTasksFlag>0</ActionTasksFlag>";
	r = _InnoApp.GetMyActivity(aml);
	XmlDocument inDom = new XmlDocument();
	inDom.LoadXml(r);
	XmlNodeList xnl_Act2s = inDom.SelectNodes("//activity_id" );	
	string str_Act2s="";
	foreach (XmlNode x in xnl_Act2s)
	{
		str_Act2s += x.InnerText + ",";
	}
	if(str_Act2s.IndexOf(strAct2Id)<0){
		throw new Exception(_InnoApp.Getl10nValue("in_ui_resources","in_GetAct2.NotAllowVote","無簽審權限"));
	}

	//取得這個Activity2
	aml = "<AML>";
	aml += "<Item type='Activity2' action='get' id='" + strAct2Id + "'>";
	aml += "</Item></AML>";
	itmR = inn.applyAML(aml);


    itmR.setProperty("map_to_property","2");
    itmR=itmR.apply("In_Fetch_ItemProperties");

	//如果是主辦,則顯示Activity2的  percent_compl,但若是協辦人員,則必須要顯示 Activity2 Assignment的 percent_compl
	itmR.setProperty("process_by_identity_label",_InnoApp.Getl10nValue("in_ui_resources","in_GetAct2.process_by_identity","本回報人員之進度"));
	itmR.setProperty("identity_type_label",_InnoApp.Getl10nValue("in_ui_resources","in_GetAct2.identity_type","主協辦"));


	string reply_mode="4";
	//判斷是否可回報進度
	//1.是主辦,是協辦:正常按鈕,為了要在in_VoteAct2內,區分出,按下 complete 的時候要更新表頭還是表身,所以區分出1,2
	//2.非主辦,是協辦:正常按鈕
	//3.是主辦,Activity2有協辦,非協辦:僅可回報完成
	//4.是主辦,Activity2無協辦:正常按鈕
	//5.非協辦也非主辦:回報錯誤訊息

	bool IsAss = false; //是協辦
	bool IsManager = false;//是主辦
	bool Act2HasAss = false;//有協辦


	string identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;

	if(identity_list.IndexOf(itmR.getProperty("managed_by_id",""))>=0)
		IsManager=true;


	Item itmAct2Ass = inn.applySQL("select id,related_id from [innovator].[Activity2_Assignment] where [innovator].[Activity2_Assignment].[source_id]='" + itmR.getID() + "'");	
	if(itmAct2Ass.getItemCount()>0)
	{
		Act2HasAss=true;		
	}
	for(int i=0;i<itmAct2Ass.getItemCount();i++)
	{
		Item tmp = itmAct2Ass.getItemByIndex(i);
		if(identity_list.IndexOf(tmp.getProperty("related_id",""))>=0)
		{
			IsAss=true;
			Current_Act2AssId = tmp.getID();
		}
	}



	if(IsManager && IsAss)
		reply_mode = "1"; //1.是主辦,是協辦:可回報進度,儲存按鈕

	if(!IsManager && IsAss)
		reply_mode = "2"; //2.非主辦,是協辦:可回報進度,儲存按鈕


	if(IsManager && Act2HasAss && !IsAss)
		reply_mode = "3"; //3.是主辦,Activity2有協辦,非協辦:不可回報進度,僅可回報完成

	if(IsManager && !Act2HasAss)
		reply_mode = "4"; //4.是主辦,Activity2無協辦:可回報進度,儲存按鈕

	if(!IsManager && !IsAss)
		throw new Exception(_InnoApp.Getl10nValue("in_ui_resources","in_GetAct2.NotAllowVote","無簽審權限"));

	itmR.setProperty("reply_mode",reply_mode);

	if(IsManager)
	{
		itmR.setProperty("identity_type_value",_InnoApp.Getl10nValue("in_ui_resources","in_GetAct2.identity_type_manager","主辦"));		
	}
	else
	{
		itmR.setProperty("identity_type_value",_InnoApp.Getl10nValue("in_ui_resources","in_GetAct2.identity_type_assignment","協辦"));		
	}	

	//抓到正確的回報進度:
	//如果是主辦,則顯示Activity2的  percent_compl,但若是協辦人員,則必須要顯示 Activity2 Assignment的 percent_compl
	if(reply_mode=="1" || reply_mode=="2")
	{
		itmAct2Ass = inn.applySQL("select percent_compl from [innovator].[Activity2_Assignment] where [innovator].[Activity2_Assignment].[id]='" + Current_Act2AssId + "'");	
		process_by_identity = itmAct2Ass.getProperty("percent_compl","0");
	}
	else
	{
		process_by_identity = itmR.getProperty("percent_compl","0");
	}
	itmR.setProperty("process_by_identity_value",process_by_identity);			

	//TimeFrame:
	string TimeFrame_value="";
	string  date_start_sched =  itmR.getProperty("date_start_sched","");
	string  date_due_sched =  itmR.getProperty("date_due_sched","");

	if(date_start_sched=="" || date_due_sched==""){
	}
	else{
	 TimeFrame_value = date_start_sched.Substring (5,5) + "~" +  date_due_sched.Substring (5,5);
	}	

	itmR.setProperty("timeframe_value",TimeFrame_value);

	//GetProjectName
	aml = "<AML>";
	aml += "<Item type='project' action='get' select='keyed_name'>";
	aml += "<project_number>" + itmR.getProperty("proj_num","") + "</project_number>";
	aml += "</Item>";
	aml += "</AML>";
	Item Prj = inn.applyAML(aml);
	itmR.setProperty("prj_name_value",Prj.getProperty("keyed_name"));

	/* //工時記錄表規則:Time Record : date_from,date_to,work_hours,work_identity 不得為 Empty
	aml = "<AML>";
	aml += "<Item type='Time Record' action='get'>";
	aml += "<source_id>" + strAct2Id + "</source_id>";
	aml += "</Item>";
	aml += "</AML>";

	Item TRs  = inn.applyAML(aml);
	string IsTRsComplete = "1";
	for(int i=0;i<TRs.getItemCount();i++)
	{
		Item TR = TRs.getItemByIndex(i);
		if(TR.getProperty("date_from","")=="" || TR.getProperty("date_to","")=="" || TR.getProperty("work_hours","")=="" || TR.getProperty("work_identity","")=="")
		{
			IsTRsComplete="0";
			break;
		}
	} */
	
	if(itmR.getProperty("date_start_act","")=="")
		itmR.setProperty("date_start_act",System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
	
	Item typeItem=inn.getItemById("ItemType",itmR.getAttribute("typeId",""));

	itmR.setProperty("inn_itemlabel",typeItem.getProperty("label",""));

	
	
	
	//產生回報進度的按鈕
	Item itmAction = inn.newItem();
	itmAction.setType("action");
	itmAction.setProperty("href","");
	itmAction.setProperty("btn_type","report");
	string strClick= "";
	itmAction.setProperty("click",strClick);	
	itmAction.setProperty("label","回報");
	itmAction.setProperty("data_toggle","modal");
	itmAction.setProperty("data_target","#vote_box");
	itmR.addRelationship(itmAction);
	
	//itmR.setProperty("time_record_complete_value",IsTRsComplete);
    itmR.setType("method");
	itmR = itmR.apply("In_AppendExtraProperties");

}
catch(Exception ex)
{	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
	string strError = ex.Message + "\n";
	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	//_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	if(ex.Source!="IOM")
	{
		//代表是這個method產生的客製錯誤訊息
		strError = ex.Message;
	}
	//throw new Exception(_InnH.Translate(strError));
	throw new Exception(strError);
}

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetAct2_N' and [Method].is_current='1'">
<config_id>FD3850B5852D48EF8FBBE7E95BE2ADEB</config_id>
<name>In_GetAct2_N</name>
<comments>提供 voteAct2的內容</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
