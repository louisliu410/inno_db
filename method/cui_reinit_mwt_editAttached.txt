if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame && workerFrame.grid && workerFrame.grid.getSelectedId()) {
		return {'cui_disabled': false};
	} else {
		return {'cui_disabled': true};
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_editAttached' and [Method].is_current='1'">
<config_id>7660248EF196412FB203D08A20C868B0</config_id>
<name>cui_reinit_mwt_editAttached</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
