if (arguments && arguments.length > 0) {
	if (arguments[0].srcElement) {
		if (parent.updateTabularViewColumn) {
			parent.updateTabularViewColumn(arguments[0].srcElement.value, document.item.getAttribute('id'));
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_UpdateTabularColumnLabel' and [Method].is_current='1'">
<config_id>AC1530C9C4784EB3B5E6FE8B0FBC7629</config_id>
<name>cmf_UpdateTabularColumnLabel</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
