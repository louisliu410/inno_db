/*
目的:複製立案單編號(in_project_filing 的 in_project_no)到專案編號(in_number)欄位
*/

Innovator inn = this.getInnovator();
string  strProjectFilingId = this.getProperty("in_project_filing","");
if(strProjectFilingId=="")
	return this;
string aml = "";
aml  = "<AML>";
aml += "<Item type='in_project_filing' actio='get' select='in_project_no' id='" + strProjectFilingId + "'>";
aml += "</Item></AML>";

Item itmProjectFiling = inn.applyAML(aml);

string sql = "Update Project set in_number='" + itmProjectFiling.getProperty("in_project_no","") + "' where id='" + this.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetProjNumber' and [Method].is_current='1'">
<config_id>911B1A0CB489416A82ACEBC5F7F42529</config_id>
<name>In_GetProjNumber</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
