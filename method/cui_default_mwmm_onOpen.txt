var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onOpenCommand) {
	workerFrame.onOpenCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onOpen' and [Method].is_current='1'">
<config_id>0E0C71DCC74241A69E109631E4A84F40</config_id>
<name>cui_default_mwmm_onOpen</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
