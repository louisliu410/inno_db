// hide select with dropdown on mouse over and click
if (event.target) {
	var curTarget = event.target;
	var checkBoxId = 'checkboxes';
	if (curTarget.id === checkBoxId) {
		return;
	}

	if (curTarget.parentNode && curTarget.parentNode.id === checkBoxId) {
		return;
	}

	if (curTarget.parentNode.parentNode && curTarget.parentNode.parentNode.id === checkBoxId) {
		return;
	}

	var classNameTemp = curTarget.className;
	if (classNameTemp === 'overSelect' || classNameTemp === 'selectBox') {
		return;
	}

	var checkboxes = document.getElementById('checkboxes');
	if (checkboxes) {
		checkboxes.style.display = 'none';
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_HideSelectInTabViewColumn' and [Method].is_current='1'">
<config_id>706CEEB747D844E991F257A4588C5747</config_id>
<name>cmf_HideSelectInTabViewColumn</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
