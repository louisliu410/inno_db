/*
目的:儲存時自動帶入使用者的部門名稱
做法:
1.取得使用者的ID
2.取得使用者的部門ID
3.更新欄位
位置:
onAfterUpdate
*/

//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string sql = "";
string strID = "";
string tempIds = "";

//1.取得使用者的ID
strID = this.getProperty("owned_by_id","");

//2.取得使用者的部門ID
tempIds = _InnH.GetOrgInfo(strID,"my_org");

//3.更新欄位
sql = "Update [In_Employee_Payment] set ";
sql += "in_dept = '" + tempIds + "'";
sql += " where id = '" + this.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ShowOrg' and [Method].is_current='1'">
<config_id>869446B448904A639299D4109087357B</config_id>
<name>In_ShowOrg</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
