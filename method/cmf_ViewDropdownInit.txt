var items = aras.evalMethod('cmf_GetViewsForCommandBars', '', inArgs);
return {
	'cui_items': items,
	'cui_style': items.length === 1 ? 'display: none' : ''
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ViewDropdownInit' and [Method].is_current='1'">
<config_id>AEC86B4F2EAC47DB86B81A999F3F4C5A</config_id>
<name>cmf_ViewDropdownInit</name>
<comments>Initializes cmf_view_dropdown CommandBarItem</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
