/*
目的:重整 Activity 的 in_config_id 與 in_itemtype
做法:
*/
string strMethodName = "In_UpdateActivityAll";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strError = "";
//string strUserId = inn.getUserID();
//string strIdentityId = inn.getUserAliases();

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;
int intCount=0;
string strWFPIDs = "";	
try
{
	Item itmActivitys = inn.newItem("Activity","get");
	itmActivitys.setProperty("state","'Active','Pending'");
	itmActivitys.setPropertyAttribute("state","condition","in");
	itmActivitys= itmActivitys.apply();
	
	for(int i=0;i<itmActivitys.getItemCount();i++)
	{
		Item itmActivity = itmActivitys.getItemByIndex(i);
		string strWFPId = itmActivity.getProperty("in_wfp_id","");
		if(strWFPId=="")
		    continue;
		intCount++;
		strWFPIDs += strWFPId + ",";
		Item itmWFP = inn.getItemById("Workflow Process",strWFPId);
		sql = "Update [Activity] set in_config_id='" + itmWFP.getProperty("in_config_id","") + "',in_itemtype='" +itmWFP.getProperty("in_itemtype") + "' where id='" + itmActivity.getID() + "'";		
		inn.applySQL(sql);
		
		if(itmActivity.getProperty("state","")=="Active")
		{
			string strCurrentActivityLabel = itmActivity.getProperty("label","");
			if(strCurrentActivityLabel=="")
				strCurrentActivityLabel = itmActivity.getProperty("name","");
			
			Item itmControlledItem = _InnH.GetInnoControlledItem(itmActivity);
			sql = "Update [" + itmControlledItem.getType().Replace(" ","_") + "] set ";
			sql += " in_current_activity_id='" + itmActivity.getID() + "'";
			sql += ",in_current_activity=N'" + strCurrentActivityLabel + "'";
			//sql += ",in_current_activity_name=N'" + itmActivity.getProperty("name","") + "'";
			sql += " where config_id='" + itmActivity.getProperty("in_config_id") + "' and is_current='1'";		
			inn.applySQL(sql);
			
			sql = "Update [Workflow_Process] set ";
			sql += " in_current_activity=N'" + strCurrentActivityLabel + "'";
			sql += ",in_current_activity_name=N'" + itmActivity.getProperty("name","") + "'";
			sql += ",in_current_activity_id=N'" + itmActivity.getID() + "'";
			sql += " where id='" + strWFPId + "'";
			inn.applySQL(sql);
			
			
		}
		
		CCO.Utilities.WriteDebug("In_UpdateActivityAll","Activity:" + (i+1).ToString() + "/" + itmActivitys.getItemCount() + ",WFP:" + itmWFP.getProperty("keyed_name"));
		
	}
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	if(strError=="")
	{
		strError = ex.Message + "\n";

		if(ex.Source=="IOM")
		{
			//代表這是非預期的錯誤
			if(aml!="")
				strError += "無法執行AML:" + aml  + "\n";

			if(sql!="")
				strError += "無法執行SQL:" + sql  + "\n";
		}
	}	
	string strErrorDetail="";
	strErrorDetail = strMethodName + ":" + strError; // + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	//strError = strError.Replace("\n","</br>");
	
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

CCO.Utilities.WriteDebug("In_UpdateActivityAll","筆數:" + intCount.ToString() + ",Workflow Process IDs:" + strWFPIDs);		
return inn.newResult("筆數:" + intCount.ToString() + ",Workflow Process IDs:" + strWFPIDs);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateActivityAll' and [Method].is_current='1'">
<config_id>16DC3A3BE0C6460FB02BE9D992DE424A</config_id>
<name>In_UpdateActivityAll</name>
<comments>重整 Activity 的 in_config_id 與 in_itemtype</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
