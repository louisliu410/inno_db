//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

//取得基礎日
string strBasisDay = this.getProperty("BasisDay","");

//取得前後天數
string strDays = this.getProperty("Days","");
int intDays = Convert.ToInt32(strDays);

//取得前後區間
string strInterval = this.getProperty("Interval","");
int intInterval = Convert.ToInt32(strInterval);

string param = "";
string strWorkDates = "";

Item itmWorkDateTime = null;

//取得工作日
switch(intDays >= 0){
    
    case true:
    
    for(int i=intInterval;i<=intDays;i++){
        param = "<mode>add</mode>";
        param += "<date>" + strBasisDay + "</date>";
        param += "<days2add>" + i + "</days2add>";
        itmWorkDateTime = inn.applyMethod("PM_handleWorkDays",param);
        strWorkDates += itmWorkDateTime.getProperty("r","").Split('T')[0] + ",";
    }
    break;
    
    case false:
    
    for(int i=intInterval;i>=intDays;i--){
        param = "<mode>add</mode>";
        param += "<date>" + strBasisDay + "</date>";
        param += "<days2add>" + i + "</days2add>";
        itmWorkDateTime = inn.applyMethod("PM_handleWorkDays",param);
        strWorkDates += itmWorkDateTime.getProperty("r","").Split('T')[0] + ",";
    }
    break;
    
    default:
    break;
}
strWorkDates = strWorkDates.Trim(',');

Item itm_R = inn.newItem("r");
itm_R.setProperty("workdays",strWorkDates);

return itm_R;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetWorkDays' and [Method].is_current='1'">
<config_id>836F8DFF292845C78F215797F3F93449</config_id>
<name>In_GetWorkDays</name>
<comments>取得工作天</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
