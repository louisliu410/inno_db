/*
目的:組合出 FDA URL,並生成 QR Code
做法:
1.取得 fdaurl variable
2.若 in_verifynumber 有值,則組合出URL與QRCODE
3.若 in_verifynumber 為空值,則清空URL與QRCODE,有助於可以更正編號,再重組URL
*/
//System.Diagnostics.Debugger.Break();

string strVerifyNumber = this.getProperty("in_verifynumber","");
if(strVerifyNumber.Length<12)
{
	this.setProperty("in_fdaurl","");
	this.setProperty("in_qrcode","");
	return this;
}

if(this.getProperty("in_qrcode","")!="")
	return this;

Innovator inn = this.getInnovator();
Item itmFdaUrl = inn.getItemByKeyedName("In_Variable","fdaurl");
if(itmFdaUrl.isError())
	throw new Exception("無FDAURL網址,請檢查[fdaurl]變數內容");

string strFDAURL = itmFdaUrl.getProperty("in_value","");	



strFDAURL = strFDAURL.Replace("#lic",strVerifyNumber.Substring(4, 8));


//1.產生QR Code,並另存成file
string strQRCodePath = CCO.Server.MapPath("../server/temp/" + this.getID() + ".jpg");
Innosoft.InnUtility.GenerateQRCode(strFDAURL ,strQRCodePath,300,300,1);		

//2.將QR Code檔案上傳至Vault中

	Item itmQRCode = inn.newItem("File","add");
	//itmQRCode.setProperty("filename","QRCode-" + this.getType() + "-" + this.getID() + ".jpg");
	itmQRCode.setProperty("filename",this.getProperty("keyed_name","") + ".jpg");

itmQRCode.attachPhysicalFile(strQRCodePath);
itmQRCode = itmQRCode.apply();

this.setProperty("in_fdaurl",strFDAURL);
this.setProperty("in_qrcode",itmQRCode.getID());

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GenerateFDAQRCode' and [Method].is_current='1'">
<config_id>D300C2564DCD49409EF2CB10E00DCE42</config_id>
<name>In_GenerateFDAQRCode</name>
<comments>Inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
