var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
var itemID = workerFrame.grid.getSelectedId();
var res = aras.setDefaultLifeCycle(workerFrame.itemTypeName, itemID);
if (!res) {
	aras.AlertError(aras.getResource('', 'mainmenu.set_lc_failed'));
	return false;
} else {
	var win = aras.uiFindWindowEx(itemID);
	if (win) {
		var itemNd = aras.getItemById(workerFrame.itemTypeName, itemID, 0);
		aras.uiReShowItemEx(itemID, itemNd);
	}

	if (workerFrame.isItemsGrid) {
		workerFrame.updateRow(aras.getItemById(workerFrame.itemTypeName, itemID, 0));
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_setDefault_LC' and [Method].is_current='1'">
<config_id>A2F90CB28A12487FA276A3DADB3D72D0</config_id>
<name>cui_default_mwmm_setDefault_LC</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
