/*
目的:
做法:
1.檢查表單是否上鎖
In_Invoice_detail_2
位置:物件動作
*/

var inn = this.getInnovator();

//1.檢查表單是否上鎖
if(this.getLockStatus() !== 0)
{
	alert(inn.applyMethod("In_Translate","<text>請先解鎖本文件再執行</text>").getResult());
	return;
}
var rel_name = "";

    if(this.getProperty("in_contract","")=="")
        rel_name = "In_Vendor_Payment_Detail";
    else
        rel_name = "In_Vendor_Payment_detail_2";

 var param= "<parent_type>"+thisItem.getType()+"</parent_type>";
    param += "<parent_id>"+thisItem.getID()+"</parent_id>";
    param += "<rel_type>" + rel_name + "</rel_type>";
    param += "<money_tax>in_invoice_tax_o</money_tax><money>in_invoice_o</money><tax>in_tax_o</tax>";
    param += "<base>money_tax</base>";

var itmApplyMethod = inn.applyMethod("In_MoneyTaxHandler",param);

this.apply("In_PaymentCompute");

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_VP_MoneyTax' and [Method].is_current='1'">
<config_id>991CB7857A10414FB60B60D13D8BB1B7</config_id>
<name>In_VP_MoneyTax</name>
<comments>公用稅額(廠商請款)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
