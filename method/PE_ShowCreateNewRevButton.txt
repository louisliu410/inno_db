if(top.aras.isLocked(document.item))
{
  hideNewRevisionButton(true);
  return;
}

if(top.aras.getItemProperty(document.item, "state", "") != "Released")
{
  hideNewRevisionButton(true);
  return;
}

var ownedById = top.aras.getItemProperty(document.item, "owned_by_id","");
if(!pb_isCurrUserMemberOfIdentityId(ownedById))
{
  hideNewRevisionButton(true);
  return;
}

var dom = top.aras.createXMLDocument();
dom.loadXML(document.item.xml);
dom.documentElement.setAttribute("action", "PE_CheckChangeItemUsage");
var res = top.aras.soapSend("ApplyMethod", dom.xml);

if(top.aras.hasFault(res.results))
{
  hideNewRevisionButton(true);
  return;
}

hideNewRevisionButton(false);

function hideNewRevisionButton(hide)
{ 
  var button = document.getElementsByName("major_rev_increment")[0];
  var text = document.getElementsByName("major_rev")[0];
  if(hide)
  {
    text.size = "4";
    button.style.display = "none";
  }
  else
  {
    text.size = "1";
    button.style.display = "inline"; 
  }
}

function pb_isCurrUserMemberOfIdentityId(identity_to_check) 
{
  if (identity_to_check === "") {return false;}
    
  var sessionIdentitites_array = top.aras.getIdentityList().split(",");
  // now we have all identities the current user (session) is member of
  // scan if given identity is in the list - if yes, return true
  for (var i=0;i<sessionIdentitites_array.length;i++)
  {
    if (identity_to_check == sessionIdentitites_array[i]) {return true;}
  }
  return false;
};


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_ShowCreateNewRevButton' and [Method].is_current='1'">
<config_id>A826776C4E8E4BD284B74D23978B696B</config_id>
<name>PE_ShowCreateNewRevButton</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
