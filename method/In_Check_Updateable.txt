/*
    這個方法假設傳入時已確定物件以確定是可換版的類型。因此不會再檢查這些東西。
    傳入參數：
        itemid: 目標物件的id
        itemtype:  目標物件的類型


*/
Innovator inn=this.getInnovator();
string strItemid=this.getProperty("itemid","no_data");
string strItemtype=this.getProperty("itemtype","no_data");
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

if(strItemid=="no_data"|| strItemtype=="no_data"){
    throw new Exception("Missing key parameter strItemid = "+strItemid +"  strItemtype  = "+strItemtype);
}

try{
    Item itmQ=inn.getItemById(strItemtype,strItemid);
    bool boolCanUpdate=false;

    /*
    Item itmR=inn.newItem("Response","whatever");
    bool boolCanUpdate=false;
    string strIsReleased=itmQ.getProperty("is_released","0");
    if(strIsReleased=="1"){
        boolCanUpdate=true;
    }else{
        boolCanUpdate=false;
        return inn.newResult(boolCanUpdate.ToString());
    }
    */
    
	
	
    int intLockStatus=itmQ.getLockStatus();
	switch(strItemtype){
		
		case "Document":
			if(itmQ.getProperty("state","")=="Released"){
				boolCanUpdate=true;
			}
		break;
		case "in_proposal":
			if(itmQ.getProperty("state","")=="In Process"){
				boolCanUpdate=true;
			}
		break;
		default : return inn.newResult(false.ToString());
	}
	
	
    if(intLockStatus==1 && boolCanUpdate){
        return inn.newResult(true.ToString());
    }else if(intLockStatus==2 && boolCanUpdate){
        return inn.newResult(false.ToString());
    }else if(intLockStatus==0&&boolCanUpdate){
        try{
            itmQ.lockItem();
            return inn.newResult(true.ToString());
        }catch(Exception exl){
            return inn.newResult(false.ToString());
        }           
    }else{
        return inn.newResult(false.ToString());
        
    }
    
 
    
    
    
    }catch(Exception eel){
    
    string strError = eel.Message + "\n";
	string strErrorDetail="";
	string strParameter="\n itemtype="+strItemtype+" , itemid= "+strItemid;
	_InnH.AddLog(strErrorDetail+strParameter,"Error");
	strError = strError.Replace("<br/>","");

	throw new Exception(_InnH.Translate(strError+strParameter));

    
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Check_Updateable' and [Method].is_current='1'">
<config_id>7FD105614298461B9EC941800D22E276</config_id>
<name>In_Check_Updateable</name>
<comments>檢查一個物件是否可以被更新。主要由InnoJSON呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
