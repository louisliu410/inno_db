var elementNodes = item.selectNodes('Relationships/Item[@type=\'cmf_ElementType\' and not(@action=\'delete\')]');

var allElementIds = '';
if (elementNodes.length > 0) {
	for (var i = 0; i < elementNodes.length; i++) {
		allElementIds += '\'' + elementNodes[i].getAttribute('id') + '\', ';
	}
	allElementIds = allElementIds.substring(0, allElementIds.length - 2);
	var filter = inDom.ownerDocument.createElement('source_id');
	filter.setAttribute('condition', 'in');
	filter.text = allElementIds;
	inDom.appendChild(filter);
} else {
	var stubFilter = {};
	stubFilter['source_id'] = {
		filterValue: aras.getItemProperty(item, 'source_id'),
		isFilterFixed: true
	};
	return stubFilter;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_TabularViewColumnFilter' and [Method].is_current='1'">
<config_id>703297F98DDE4E7F8376773DD8146AA4</config_id>
<name>cmf_TabularViewColumnFilter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
