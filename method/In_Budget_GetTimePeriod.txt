/*
目的:取得預算表模組所需要的人事費計算起訖時間與非人事費計算起訖時間
邏輯:如果staff_cost_period 是 21-20, 則起訖為 上個月21~本月20,但如果是 01-last, 則起訖為 本月01~本月最後一天
接收參數:<yearmonth>本月</yearmonth>
Return:
<staff_startdate>
<staff_enddate>
<other_startdate>
<other_enddate>
做法:
1.取得 staff_cost_period,non_staff_cost_period 判斷若結束為last,則開始月份為本月,否則為上月
2.若結束為last ,則切換為本月最後一日
*/

string strMethodName = "In_Budget_GetTimePeriod";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

try
{
	string yearmonth = this.getProperty("yearmonth",System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM"));
	DateTime dtYearMonth = Convert.ToDateTime(yearmonth + "-01T00:00:00");

	string strStaff_StartDay = _InnH.GetInVariable("in_budget").getProperty("staff_cost_period","21-20").Split('-')[0];
	string strStaff_EndDay = _InnH.GetInVariable("in_budget").getProperty("staff_cost_period","21-20").Split('-')[1];
	DateTime dtStaff_EndDate;
	DateTime dtStaff_StartDate;
	if(strStaff_EndDay=="last")
	{
		//代表是當月1~last
		dtStaff_StartDate = Convert.ToDateTime(dtYearMonth.ToString("yyyy-MM")+"-"+ strStaff_StartDay + "T00:00:00");
		dtStaff_EndDate = Convert.ToDateTime(dtYearMonth.AddMonths(1).ToString("yyyy-MM")+"-01T00:00:00").AddMinutes(-1);
	}
	else
	{
		//代表是上個月幾號到這個月幾號
		dtStaff_StartDate = Convert.ToDateTime(dtYearMonth.AddMonths(-1).ToString("yyyy-MM")+"-"+ strStaff_StartDay + "T00:00:00");
		dtStaff_EndDate = Convert.ToDateTime(dtYearMonth.ToString("yyyy-MM")+"-" + strStaff_EndDay + "T23:59:59");		
	}

	string strOther_StartDay = _InnH.GetInVariable("in_budget").getProperty("non_staff_cost_period","01-last").Split('-')[0];
	string strOther_EndDay = _InnH.GetInVariable("in_budget").getProperty("non_staff_cost_period","01-last").Split('-')[1];
	DateTime dtOther_EndDate;
	DateTime dtOther_StartDate;
	if(strOther_EndDay=="last")
	{
		//代表是當月1~last
		dtOther_StartDate = Convert.ToDateTime(dtYearMonth.ToString("yyyy-MM")+"-"+ strOther_StartDay + "T00:00:00");
		dtOther_EndDate = Convert.ToDateTime(dtYearMonth.AddMonths(1).ToString("yyyy-MM")+"-01T00:00:00").AddMinutes(-1);
	}
	else
	{
		//代表是上個月幾號到這個月幾號
		dtOther_StartDate = Convert.ToDateTime(dtYearMonth.AddMonths(-1).ToString("yyyy-MM")+"-" +strOther_StartDay + "T00:00:00");
		dtOther_EndDate = Convert.ToDateTime(dtYearMonth.ToString("yyyy-MM")+"-" + strOther_EndDay + "T23:59:59");		
	}

	itmR = inn.newItem();
	itmR.setProperty("staff_startdate",dtStaff_StartDate.ToString("yyyy-MM-ddTHH:mm:ss"));
	itmR.setProperty("staff_enddate",dtStaff_EndDate.ToString("yyyy-MM-ddTHH:mm:ss"));
	itmR.setProperty("other_startdate",dtOther_StartDate.ToString("yyyy-MM-ddTHH:mm:ss"));
	itmR.setProperty("other_enddate",dtOther_EndDate.ToString("yyyy-MM-ddTHH:mm:ss"));

}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Budget_GetTimePeriod' and [Method].is_current='1'">
<config_id>47740E195980435AA999162FD5885913</config_id>
<name>In_Budget_GetTimePeriod</name>
<comments>取得預算表模組所需要的人事費計算起訖時間與非人事費計算起訖時間</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
