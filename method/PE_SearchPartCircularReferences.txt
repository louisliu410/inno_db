Item sp = this.newItem("SQL", "SQL PROCESS");
sp.setProperty("name", "PE_SearchPartCircularReferences");
sp.setProperty("PROCESS", "CALL");
sp.setProperty("ARG1", "1000");
Item res = sp.apply();

return res;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_SearchPartCircularReferences' and [Method].is_current='1'">
<config_id>75E9C2DB990C4D759ACB03BDFFC0D247</config_id>
<name>PE_SearchPartCircularReferences</name>
<comments>To use in PE_SearchPartCircularReferences Report</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
