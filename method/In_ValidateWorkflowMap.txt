/*
目的:簽審機制優化
做法:
1.取得簽審人員為Inn_Assignment的流程圖任務節點的ID
1.2更新自動分配的欄位
2.取得狀態推動有值的流程圖任務節點的ID
2.2更新推動的欄位
3.取得預設路徑無值的後續路徑的ID
4.取得路徑條件有值的後續路徑的ID
4.2更新條件路徑的欄位
5.取得路徑條件無值的後續路徑的ID並檢查路徑條件是否有值
6.檢查是否為自動節點
位置:
onAfterAdd,onAfterUpdate
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string aml = "";
string sql = "";

string strSourceID = "";
string strAutoassignID ="";
string strPromoteID = "";
string strCriteriapathID = "";

Item itmWorkflowMapPaths = null;
Item itmActivityTemplates = null;
Item itmSQL = null;

string strNotValueID = "";
string strDefaultNotValueError = "";
string strPathError = "";
string strActivityError = "";

//取得流程關卡
aml = "<AML>";
aml += "<Item type='Workflow Map Activity' action='get'>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";
Item itmWorkflowMapActivities = inn.applyAML(aml);
for(int i=0;i<itmWorkflowMapActivities.getItemCount();i++){
    Item itmWorkflowMapActivity = itmWorkflowMapActivities.getItemByIndex(i);
    
    //取得流程關卡的ID
    strSourceID += itmWorkflowMapActivity.getProperty("related_id","") + ",";
}
strSourceID = strSourceID.Trim(',');

//取得簽審人員
aml = "<AML>";
aml += "<Item type='Activity Template Assignment' action='get'>";
aml += "<source_id condition='in'>" + strSourceID + "</source_id>";
aml += "<related_id>";
aml += "<Item type='Identity' actio='get'>";
aml += "<name>Inn_Assignment</name>";
aml += "</Item>";
aml += "</related_id>";
aml += "</Item></AML>";
Item itmActivityTemplateAssignments = inn.applyAML(aml);
for(int i=0;i<itmActivityTemplateAssignments.getItemCount();i++){
    Item itmActivityTemplateAssignment = itmActivityTemplateAssignments.getItemByIndex(i);
    
    //1.取得簽審人員為Inn_Assignment的流程圖任務節點的ID
    strAutoassignID += itmActivityTemplateAssignment.getProperty("source_id","") + ",";
}
strAutoassignID = strAutoassignID.Trim(',');

//取得狀態推動
aml = "<AML>";
aml += "<Item type='Activity Template Transition' action='get'>";
aml += "<source_id condition='in'>" + strSourceID + "</source_id>";
aml += "</Item></AML>";
Item itmActivityTemplateTransitions = inn.applyAML(aml);
for(int i=0;i<itmActivityTemplateTransitions.getItemCount();i++){
    Item itmActivityTemplateTransition = itmActivityTemplateTransitions.getItemByIndex(i);
    
    //2.取得狀態推動有值的流程圖任務節點的ID
    strPromoteID += itmActivityTemplateTransition.getProperty("source_id","") + ",";
}
strPromoteID = strPromoteID.Trim(',');

//取得流程圖任務節點
aml = "<AML>";
aml += "<Item type='Activity Template' action='get'>";
aml += "<id condition='in'>" + strSourceID + "</id>";
aml += "</Item></AML>";
itmActivityTemplates = inn.applyAML(aml);
for(int i=0;i<itmActivityTemplates.getItemCount();i++){
    Item itmActivityTemplate = itmActivityTemplates.getItemByIndex(i);
    
    //取得自動節點有值的流程圖任務節點的ID
    if(itmActivityTemplate.getProperty("is_auto","") == "1"){
        
        //3.取得預設路徑無值的後續路徑的ID
        aml = "<AML>";
        aml += "<Item type='Workflow Map Path' action='get'>";
        aml += "<source_id>" + itmActivityTemplate.getID() + "</source_id>";
        aml += "<is_default>0</is_default>";
        aml += "</Item></AML>";
        itmWorkflowMapPaths = inn.applyAML(aml);
        for(int j=0;j<itmWorkflowMapPaths.getItemCount();j++){
            Item itmWorkflowMapPath = itmWorkflowMapPaths.getItemByIndex(j);
            
            string strDefaultNotValueID = "";
            
            if(!strNotValueID.Contains(itmWorkflowMapPath.getID())){
                strNotValueID += itmWorkflowMapPath.getProperty("source_id","") + ",";
                
                strDefaultNotValueID = "節點名稱: " + itmActivityTemplate.getProperty("label","") + "</br>";
                strDefaultNotValueError += strDefaultNotValueID;
            }
        }
    }
}
if(strDefaultNotValueError != ""){
    strDefaultNotValueError = "自動節點後無預設路徑:</br>" + strDefaultNotValueError;
}

//取得後續路徑
aml = "<AML>";
aml += "<Item type='Workflow Map Path' action='get'>";
aml += "<source_id condition='in'>" + strSourceID + "</source_id>";
aml += "<in_criteria condition='is not null'></in_criteria>";
aml += "</Item></AML>";
itmWorkflowMapPaths = inn.applyAML(aml);
for(int j=0;j<itmWorkflowMapPaths.getItemCount();j++){
    Item itmWorkflowMapPath = itmWorkflowMapPaths.getItemByIndex(j);
    
    //4.取得路徑條件有值的後續路徑的ID
    if(!strCriteriapathID.Contains(itmWorkflowMapPath.getProperty("source_id",""))){
        strCriteriapathID += itmWorkflowMapPath.getProperty("source_id","") + ",";
    }
}
strCriteriapathID = strCriteriapathID.Trim(',');

if(strCriteriapathID != ""){
    
    //5.取得路徑條件無值的後續路徑的ID並檢查路徑條件是否有值
    aml = "<AML>";
    aml += "<Item type='Workflow Map Path' action='get'>";
    aml += "<source_id condition='in'>" + strCriteriapathID + "</source_id>";
    aml += "<in_criteria condition='is null'></in_criteria>";
    aml += "</Item></AML>";
    itmWorkflowMapPaths = inn.applyAML(aml);
    for(int j=0;j<itmWorkflowMapPaths.getItemCount();j++){
        Item itmWorkflowMapPath = itmWorkflowMapPaths.getItemByIndex(j);
        
        string strPath = "";
        
        aml = "<AML>";
        aml += "<Item type='Activity Template' action='get'>";
        aml += "<id>" + itmWorkflowMapPath.getProperty("source_id","") + "</id>";
        aml += "</Item></AML>";
        Item itmActivityTemplate = inn.applyAML(aml);
        strPath = "節點名稱: " + itmActivityTemplate.getProperty("label","") + " , " + "標籤名稱: " + itmWorkflowMapPath.getProperty("label","") + "</br>";
        strPathError += strPath;
    }
    //6.檢查是否為自動節點
    aml = "<AML>";
    aml += "<Item type='Activity Template' action='get'>";
    aml += "<id condition='in'>" + strCriteriapathID + "</id>";
    aml += "<is_auto>0</is_auto>";
    aml += "</Item></AML>";
    itmActivityTemplates = inn.applyAML(aml);
    for(int m=0;m<itmActivityTemplates.getItemCount();m++){
        Item itmActivityTemplate = itmActivityTemplates.getItemByIndex(m);
        string strActivity = "";
        strActivity = "節點名稱: " + itmActivityTemplate.getProperty("label","") + "</br>";
        strActivityError += strActivity;
    }
}
if(strActivityError != ""){
    strActivityError = "條件節點未設為自動節點::</br>" + strActivityError;
}
if(strPathError != ""){
    strPathError = "條件路徑未設定條件:</br>" + strPathError;
}

strSourceID = strSourceID.Replace(",","','");
strAutoassignID = strAutoassignID.Replace(",","','");
strPromoteID = strPromoteID.Replace(",","','");
strCriteriapathID = strCriteriapathID.Replace(",","','");

if(strDefaultNotValueError != "" || strActivityError !="" || strPathError != ""){
    return inn.newError(strDefaultNotValueError + "<br>" + strActivityError + "<br>" + strPathError);
}

//清除所有自訂的欄位
sql = "UPDATE [INNOVATOR].[ACTIVITY_TEMPLATE] SET";
sql += " [IN_IS_AUTOASSIGN]='0'";
sql += ",[IN_IS_CRITERIAPATH]='0'";
sql += ",[IN_IS_PROMOTE]='0'";
sql += " WHERE ID IN('" + strSourceID + "')";
itmSQL = inn.applySQL(sql);

//1.2更新自動分配的欄位
sql = "UPDATE [INNOVATOR].[ACTIVITY_TEMPLATE] SET";
sql += " [IN_IS_AUTOASSIGN]='1'";
sql += " WHERE ID IN('" + strAutoassignID + "')";
itmSQL = inn.applySQL(sql);

//2.2更新推動的欄位
sql = "UPDATE [INNOVATOR].[ACTIVITY_TEMPLATE] SET";
sql += " [IN_IS_PROMOTE]='1'";
sql += " WHERE ID IN('" + strPromoteID + "')";
itmSQL = inn.applySQL(sql);

//4.2更新條件路徑的欄位
sql = "UPDATE [INNOVATOR].[ACTIVITY_TEMPLATE] SET";
sql += " [IN_IS_CRITERIAPATH]='1'";
sql += " WHERE ID IN('" + strCriteriapathID + "')";
itmSQL = inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ValidateWorkflowMap' and [Method].is_current='1'">
<config_id>34C300394C864423A48B795D24FB808C</config_id>
<name>In_ValidateWorkflowMap</name>
<comments>檢查 Workflow Map 的正確性</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
