string strMethodName = "In_PerlWorkRecord_time_AfterDel";
/*
目的:在工時卡操作砍掉工作紀錄時,順便砍掉相對應的表身工時紀錄,
之後會再連動In_TimeRecord 的 afterDelete 功能,將TimeSheetAuditing 的表身一併刪除
位置:In_PerlWorkRecord_time onAfterDelete
需搭配 In_PerlWorkRecord_time_BeforeDel
*/
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

try
{
	string strTimeRecId = (string)RequestState["PerlWorkRecord_time_related_id"];
	aml = "<AML>";
	aml += "<Item type='In_TimeRecord' action='delete' where=\"[In_TimeRecord].[id]='" + strTimeRecId + "'\">";	
	aml += "</Item>";
	aml += "</AML>";

	itmR = inn.applyAML(aml);

}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);



	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_PerlWorkRecord_time_AfterDel' and [Method].is_current='1'">
<config_id>2521666F5922475183866D06A950C351</config_id>
<name>In_PerlWorkRecord_time_AfterDel</name>
<comments>在工時卡操作砍掉工作紀錄時,順便砍掉相對應的表身工時紀錄</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
