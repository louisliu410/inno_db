/*
目的:將取得的Context主檔與關連的特殊屬性再後處理一次
做法:
1.itemtype類:補上 keyed_name 與 itemtype
2.identity要補上 tel, email
3.user要補上 tel, email
*/ 
string strMethodName = "In_AppendExtraProperties";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

try
{
    
	//將ItemType的 keyed_name 都拉出來變成一個獨立的 property, 屬性名稱為: property_keyed_name
	XmlNodeList xnProperties = this.dom.SelectNodes("Item/*");		
	AppendProperties(xnProperties,this,inn);

	//關聯
	Item itmRels = this.getRelationships();
	for(int i=0;i<itmRels.getItemCount();i++)
	{
		Item itmRel = itmRels.getItemByIndex(i);
		XmlNodeList xnRelProperties = itmRel.node.SelectNodes("*");
		foreach(XmlNode n in xnRelProperties)
		{
			//為了相容新舊app, 因此需要將舊的app的itemtype轉換成新的type
			if(n.Attributes["itemtype"]!=null)
			{
				XmlAttribute attr = itmRel.dom.CreateAttribute("type");
				n.Attributes.Append(attr);
				n.Attributes["type"].Value = n.Attributes["itemtype"].Value;
				n.Attributes.Remove(n.Attributes["itemtype"]); 
			}
		}
		AppendProperties(xnRelProperties,itmRel,inn);
	}

}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;

}
private Item AppendProperties(XmlNodeList xnProperties,Item Context,Innovator inn)
{
    foreach(XmlNode n in xnProperties)
	{
		if(n.InnerText.StartsWith("#"))
		{
			switch(n.InnerText)
			{
				case "#FF0000":
					Context.setProperty(n.Name + ".colorclass","red");
					break;
				case "#00FF00":
					Context.setProperty(n.Name + ".colorclass","green");
					break;
				case "#FFFF00":
					Context.setProperty(n.Name + ".colorclass","yellow");
					break;
				case "#FFFFFF":
					Context.setProperty(n.Name + ".colorclass","gray");
					break;
				default:
					Context.setProperty(n.Name + ".colorclass","info");
					break;
				
			}
		}
		XmlAttributeCollection attrAttributes = n.Attributes;
		foreach (XmlAttribute at in attrAttributes)
		{
			Context.setProperty(n.Name + "." + at.LocalName,Context.getPropertyAttribute(n.Name,at.LocalName,""));
			if(Context.getPropertyAttribute(n.Name,at.LocalName,"")=="in_file" && at.LocalName=="type")
			{
				//特別處理 in_file, 因為 deliverable 不會抓到 in_file 內的 in_file property
				if(n.InnerText!="")
				{
					Item itmInFile = inn.getItemById("in_file",n.InnerText);
					string strFileId = itmInFile.getProperty("in_file","");
					Context.setProperty(n.Name + ".fileid",strFileId);
				}
			}
		}
		
		string strUserID ="";
		
		/*
		 if(Context.getPropertyAttribute(n.Name,"type","")=="Identity")
		{
			string s =  Context.getProperty(n.Name,"");
			if(s!="")
			{
				Item itmIdentity = inn.getItemById("identity", Context.getProperty(n.Name));
				strUserID = itmIdentity.getProperty("in_user","");				
			}
			
		}		
		if(Context.getPropertyAttribute(n.Name,"type","")=="User")
		{			
			strUserID = Context.getProperty(n.Name,"");
		}
		if(strUserID!="")
		{
			Item itmUser = inn.getItemById("User",strUserID);
			string strEmail = itmUser.getProperty("email","");
			string strCell = itmUser.getProperty("cell","");			
			Context.setProperty(n.Name + ".email",strEmail);
			Context.setProperty(n.Name + ".cell",strCell);
			Context.setProperty(n.Name + ".keyed_name",itmUser.getProperty("keyed_name"));
		}
    */
	}
	
	return Context;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AppendExtraProperties' and [Method].is_current='1'">
<config_id>B4C623BE1F4244619345F1823A557843</config_id>
<name>In_AppendExtraProperties</name>
<comments>將取得的Context主檔與關連的特殊屬性再後處理一次</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
