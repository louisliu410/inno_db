// CommandBarItem is visible if type is CMF type
var data = aras.MetadataCache.GetContentTypeByDocumentItemType(inArgs.itemTypeId);
if (data && data.results) {
	data = data.results.selectSingleNode('//ids/text()');
} else {
	data = null;
}
return {
	'cui_visible': data && data.text.length > 0 && data.text !== '00000000000000000000000000000000'
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_InitCuiItem' and [Method].is_current='1'">
<config_id>6954D3163F154107B3B18B46DE8C7D62</config_id>
<name>cmf_InitCuiItem</name>
<comments>CommandBarItem setup</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
