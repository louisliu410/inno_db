/*
目的:將Related Part 的資訊謄寫到 In_Quote_Details 內
位置:In_Quote_Details 的 onBeforeAdd
做法:
1.取得Related PartID
2.執行CopyRelatedPartToQuoteDetail
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmUpdatedQuoteDetail=null;
	
try
{
	itmUpdatedQuoteDetail = _InnH.CopyRelatedPartToQuoteDetail(this);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmUpdatedQuoteDetail;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CopyRelatedPartToQuoteDetail' and [Method].is_current='1'">
<config_id>25705585E5CC4F8F88F41162083F6E6A</config_id>
<name>In_CopyRelatedPartToQuoteDetail</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
