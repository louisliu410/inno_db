/*
目的:執行回傳回來的aml
做法:直接apply
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
Item itmR;
string strAML="";
try
{
	strAML = this.dom.SelectSingleNode("//AML").OuterXml;
	itmR = inn.applyAML(strAML);
}
catch(Exception ex)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "匯入失敗:無法執行AML:";
	}
	strError = strError + ",AML:" + strAML;
	strErrorDetail = strError + "\n" + ex.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	throw new Exception(strError);
}	
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_MergeAML' and [Method].is_current='1'">
<config_id>9BF18AE16EAE4E06A96B24AF60CA1873</config_id>
<name>In_MergeAML</name>
<comments>執行aml</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
