if (!aras.clearCache()) {
	aras.AlertError(aras.getResource('', 'imports_core.clear_cache_failed'));
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='clear_cache' and [Method].is_current='1'">
<config_id>8A58E2DC960449198A91E2A91EDB8F1C</config_id>
<name>clear_cache</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
