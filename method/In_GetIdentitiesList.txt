/*
目的:取得本次登入的IdentitiesList
做法:
1.提供Aras.Server.Security.Permissions.Current.IdentitiesList
*/

Innovator inn = this.getInnovator();
return inn.newResult(Aras.Server.Security.Permissions.Current.IdentitiesList);


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetIdentitiesList' and [Method].is_current='1'">
<config_id>EF2A6EDD39A540D7A663C59E3BBC684B</config_id>
<name>In_GetIdentitiesList</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
