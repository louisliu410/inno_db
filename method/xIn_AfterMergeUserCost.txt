/*
目的:依據HR匯入的薪資表計算出個人時薪
做法:

計算每張工時卡的成本
建立公司之間的應收應付
將該月份的實際工時與費用更新至人事預算頁簽中
該月分的預算表全部重整
產生該月份WIP EXCEL


*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.InnoERP _InnERP = new Innosoft.InnoERP(inn);
string aml = "";
string sql = "";
Item itmR = null;
string strErrorMessage = "";
string strCompany = "";
try
{
    
	string strYearMonth = this.getProperty("yearmonth",System.DateTime.Now.AddMonths(-1).ToString("yyyy-MM"));
	strYearMonth = "2017-02";
    
	
	
	//計算每張工時卡的成本
	strErrorMessage = "計算工時卡成本時出錯(" + strYearMonth + ")";
	Item itmR1 = inn.applyMethod("In_Calculate_TimeRecordCost","<yearmonth>" + strYearMonth + "</yearmonth>");
	if(itmR1.isError())
    	throw new Exception(itmR1.getErrorString());
	
	//建立公司之間的應收應付
	strErrorMessage = "建立公司之間的應收應付時出錯(" + strYearMonth + ")";
	Item itmR2 = inn.applyMethod("In_Create2WayPayment","<yearmonth>" + strYearMonth + "</yearmonth>");
	if(itmR2.isError())
		throw new Exception(itmR2.getErrorString());
	
	//將該月份的實際工時與費用更新至人事預算頁簽中
	strErrorMessage = "將該月份的實際工時與費用更新至人事預算頁簽中時出錯(" + strYearMonth + ")";
	Item itmR3 = inn.applyMethod("In_CreateWIP","<yearmonth>" + strYearMonth + "</yearmonth>");
	if(itmR3.isError())
		throw new Exception(itmR3.getErrorString());
	
	return this;
		
	//該月分的預算表全部重整
	strErrorMessage = "重整該月分的預算表全部重整出錯(" + strYearMonth + ")";
	Item itmR4 = inn.applyMethod("In_BudgetAggreate_S_All","<yearmonth>" + strYearMonth + "</yearmonth>");
	if(itmR4.isError())
		throw new Exception(itmR4.getErrorString());	
		
		
	//產生該月份WIP EXCEL
	strErrorMessage = "產生該月份WIP EXCEL出錯(" + strYearMonth + ")";
	Item itmR5 = inn.applyMethod("In_CreateWIPExcel_All","<yearmonth>" + strYearMonth + "</yearmonth>");
	if(itmR5.isError())
		throw new Exception(itmR5.getErrorString());		
    


}
catch(Exception ex)
{   
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

    string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
    string strErrorDetail="";
    if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
    {
        strError = "匯入主檔失敗:無法執行AML:" + aml ;
    }
	//if(strErrorMessage!="")
	//	strError = strErrorMessage + itmR.getErrorString();
    strErrorDetail = strError + "\n" + ex.ToString() + ":" +  aml ;
    Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
    return inn.newError(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xIn_AfterMergeUserCost' and [Method].is_current='1'">
<config_id>C6C9B1C5429646CD929F0AF00953B938</config_id>
<name>xIn_AfterMergeUserCost</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
