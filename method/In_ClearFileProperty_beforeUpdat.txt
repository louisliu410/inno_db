/*
目的:
1.如果原始檔案被置換,則過程word檔案與PDF都要被清除
2.如果過程word檔被修改,則PDF檔案要被清除
3.如果 藥証識別碼(in_drug_approval)為 「送證中」,則發出通知給藥証的負責人(Owner)
位置:onbeforeupdate
做法:
1.如果in_native_file 不是 null, 代表本次有修改,則將in_word_file與in_modify_file都清空
2.如果in_word_file 不是 null, 代表本次有修改,則將in_modify_file都清空
*/
//In_ClearFileProperty_beforeUpdat
//System.Diagnostics.Debugger.Break();
string pre_in_native_file = (string)RequestState["in_native_file"];
string pre_in_word_file = (string)RequestState["in_word_file"];
string aml  = "";
Innovator inn = this.getInnovator();

if(pre_in_native_file==null)
	return this;

if(this.getProperty("in_native_file","")=="")
	return this;

if(pre_in_native_file!=this.getProperty("in_native_file",""))
{
	this.setProperty("in_word_file","");
	this.setProperty("in_modify_file","");
	this.setPropertyAttribute("in_word_file","is_null","1");
	this.setPropertyAttribute("in_modify_file","is_null","1");
	
	
	//3.如果 藥証識別碼(in_drug_approval)為 「送證中」,則發出通知給藥証的負責人(Owner)
	if(this.getProperty("in_drug_approval","")=="送證中")
	{
		aml = "<AML>";
		aml += "<Item type='In_Drug_Lic_Deliverable' action='get' select='source_id(owned_by_id)'>";
		aml += "<related_id>" + this.getID() + "</related_id>";
		aml += "</Item></AML>";
		
		Item itmDrugLicDeliverable = inn.applyAML(aml);
		if(itmDrugLicDeliverable.isError())
			return this;
			
		string strDrugLicOwnerId = itmDrugLicDeliverable.getPropertyItem("source_id").getProperty("owned_by_id","");
		if(strDrugLicOwnerId == "")
			return this;
			
		Item itmDrugLicOwner = inn.getItemById("Identity",strDrugLicOwnerId);
		
		Item email_msg = this.newItem("Email Message","get");
		email_msg.setProperty("name","In_DocNativeFileChanged");
		email_msg =email_msg.apply();
		
		if( this.email( email_msg, itmDrugLicOwner) == false )
		{
			//Error handling
			int i=0;
		}
		
	}
}

if(pre_in_word_file!=this.getProperty("in_word_file",""))
{
	this.setProperty("in_modify_file","");
	this.setPropertyAttribute("in_modify_file","is_null","1");
}


return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ClearFileProperty_beforeUpdat' and [Method].is_current='1'">
<config_id>43F5253997784314AB03A9C8A673075C</config_id>
<name>In_ClearFileProperty_beforeUpdat</name>
<comments>inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
