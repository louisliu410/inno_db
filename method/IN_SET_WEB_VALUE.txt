//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string sql = "";
string strID = "";
Item itmSQL = null;

sql = "SELECT [ID],[IN_WEB_LIST],[IN_WEB_RELATIONSHIP],[IN_WEB_VIEW] FROM [Inno-pure].[innovator].[PROPERTY]";
itmSQL = inn.applySQL(sql);

//IN_WEB_LIST
for(int i=0;i<itmSQL.getItemCount();i++)
{
    if(itmSQL.getItemByIndex(i).getProperty("in_web_list","") == ""){
        strID += "'" + itmSQL.getItemByIndex(i).getID() + "',";
    }
}
strID = strID.Trim(',');

if(strID != "")
{
    sql = "UPDATE [Inno-pure].[innovator].[PROPERTY] set in_web_list = '0' WHERE [ID] IN(" + strID + ")";
    inn.applySQL(sql);
    strID = "";
}

//IN_WEB_RELATIONSHIP
for(int j=0;j<itmSQL.getItemCount();j++)
{
    if(itmSQL.getItemByIndex(j).getProperty("in_web_relationship","") == ""){
        strID += "'" + itmSQL.getItemByIndex(j).getID() + "',";
    }
}
strID = strID.Trim(',');

if(strID != "")
{
    sql = "UPDATE [Inno-pure].[innovator].[PROPERTY] set in_web_relationship = '0' WHERE [ID] IN(" + strID + ")";
    inn.applySQL(sql);
    strID = "";
}

//IN_WEB_VIEW
for(int k=0;k<itmSQL.getItemCount();k++)
{
    if(itmSQL.getItemByIndex(k).getProperty("in_web_view","") == ""){
        strID += "'" + itmSQL.getItemByIndex(k).getID() + "',";
    }
}
strID = strID.Trim(',');

if(strID != "")
{
    sql = "UPDATE [Inno-pure].[innovator].[PROPERTY] set in_web_view = '0' WHERE [ID] IN(" + strID + ")";
    inn.applySQL(sql);
    strID = "";
}

sql = "SELECT [ID],[IN_WEB_LIST] FROM [Inno-pure].[innovator].[RELATIONSHIPTYPE]";
itmSQL = inn.applySQL(sql);

//IN_WEB_LIST
for(int l=0;l<itmSQL.getItemCount();l++)
{
    if(itmSQL.getItemByIndex(l).getProperty("in_web_list","") == ""){
        strID += "'" + itmSQL.getItemByIndex(l).getID() + "',";
    }
}
strID = strID.Trim(',');

if(strID != "")
{
    sql = "UPDATE [Inno-pure].[innovator].[RELATIONSHIPTYPE] set in_web_list = '0' WHERE [ID] IN(" + strID + ")";
    inn.applySQL(sql);
    strID = "";
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='IN_SET_WEB_VALUE' and [Method].is_current='1'">
<config_id>18AD62E8885B41A3B48456D9E043A714</config_id>
<name>IN_SET_WEB_VALUE</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
