if (!aras || !aras['setup_query'] || !aras['setup_query'].StartItem) {
	return;
}

if (aras['setup_query'].StartItem == 'inbasket') {
	return 'InBasket Task';
}

var arr = aras['setup_query'].StartItem.split(':');
var type = arr[0];
var id = arr[1];
if (type && !id) {
	return type;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='selectStartPage' and [Method].is_current='1'">
<config_id>5EED7DFDF5C04571BB42AA0A516B2E92</config_id>
<name>selectStartPage</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
